import tikzplotlib
import matplotlib.pyplot as plt
import json

def export_figs(label):
    plt.savefig(f'./graphs/{label}.png')
    tikzplotlib.save(f'./graphs/{label}_src.tex', extra_axis_parameters=['axis equal image'])

f = open('./timing_result_6.json', 'r')
raw_content = f.read()
timing_result = json.loads(raw_content)

X = []
Y1 = []
Y2 = []
Y3 = []
for row in timing_result['data']:
    X.append(row['nb_of_points'])
    Y1.append(row['naive'])
    Y2.append(row['kd_tree'])
    Y3.append(row['qt_tree'])

fig, axs = plt.subplots(1, 1)
print(axs)

axs.plot(X, Y1, 'b')
axs.plot(X, Y2, 'r')
axs.plot(X, Y3, 'g')

axs.plot(X, Y1, 'bd', markersize=5)
axs.plot(X, Y2, 'rd', markersize=5)
axs.plot(X, Y3, 'gd', markersize=5)
axs.grid(True)

plt.legend(['Algo naïf', 'Algo kd-tree', 'Algo quad-tree'])

axs.set_title("Comparaison d'algorithmes de detection de collisions en 2 dimensions")
axs.set_xlabel("Nombre de points")
axs.set_ylabel("Durée d'execution CPU (en sec)")
axs.set_xscale('log')
axs.set_yscale('log')

export_figs('cmp5_new_log')
plt.show()
