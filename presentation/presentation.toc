\babel@toc {french}{}\relax 
\beamer@sectionintoc {1}{Contexte}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Le problème}{4}{0}{1}
\beamer@subsectionintoc {1}{2}{Abstraction et travail en 2D}{6}{0}{1}
\beamer@subsectionintoc {1}{3}{Différence a priori ou a posteriori}{7}{0}{1}
\beamer@subsectionintoc {1}{4}{Un premier algorithme}{8}{0}{1}
\beamer@sectionintoc {2}{Structures et algorithmes pour l'indexation spatiale}{9}{0}{2}
\beamer@subsectionintoc {2}{1}{Les arbres k-d (K-d Trees)}{10}{0}{2}
\beamer@subsectionintoc {2}{2}{Les arbres quaternaires (Quad-Tree)}{19}{0}{2}
\beamer@subsectionintoc {2}{3}{Comparaisons}{24}{0}{2}
\beamer@sectionintoc {3}{Structures et algorithmes pour le mouvement}{25}{0}{3}
\beamer@subsectionintoc {3}{1}{La technique "Balayer et émonder"}{26}{0}{3}
\beamer@subsectionintoc {3}{2}{Boîtes englobantes adaptés à la vitesse}{27}{0}{3}
\beamer@sectionintoc {4}{Conclusion}{27}{0}{4}
