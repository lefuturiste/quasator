# Présentation

## Contexte

### Le problème pratique : collision entre satellites

Rappel rapide de la problématique principale.
Illustration de la problématique

Slide 1: evolution du nb d'objets
- Graphique: Evolution du nombre d'objets en orbite basse par rapport au temps. https://commons.wikimedia.org/wiki/File:Number-Space-Debris-5-janvier-2021-fr.png
- https://commons.wikimedia.org/wiki/File:The_growth_of_all_tracked_objects_in_space_over_time_(space_debris_and_satellites).png
- Graphique: Evolution du nombre d'événements de quasi collisions en LEO
- Image: Cas d'étude collision de 2009

Slide 2:
- https://commons.wikimedia.org/wiki/File:Satellite_break-up_ESA375611.tiff

### Abstraction de la détection de collision

Introduction des variables
N particules représentés par des points avec des coordonées

### Choix de la méthode *a priori* ou *a posteriori*

On explique pourquoi le mode à posteriori est le plus adapté à notre cas:

Problème de modélisation de la trajectoire dynamique.

### Quel est le problème à résoudre ?

Besoin : Pour une particule P donnée, trouver les particules comprisent dans un rayon donnée autour de la particule P.

### L'algorithme naif

Analyser la distance entre chaque paire.
Très simple à coder mais complexité temporelle en n^2.

## Plus loin : Quelques structures et algorithmes pour l'indexation spatiale

Principe général : Partionner l'espace -> "diviser pour régner"

### Les arbres k-d (kd-tree)

- présentation de la construction de l'arbre

(image: exemple de points partitioné selon un arbre kd)

- présentation de l'algorithme de recherche des plus proches voisins

### Les arbres quaternaires

- présentation de la construction

(image: même exemple partitioné selon un arbre quaternaire)

- présentation de l'algorithme de recherche des plus proches voisins

### Optimisation des arbres quaternaires

- optimisation par rapport au nombre de feuilles (bucket size)

(image: durée de la recherche en fonction du nombre de feuilles autorisés)

### Comparaison de la complexité temporelle de ces deux approches

(image: comparaison entre {naif, arbre kd, quaternaire} de la durée de recherche en fonction du nombre de points)

## Plus loin : Concevoir une structure et un algorithme spécifique au mouvement

### La technique "Balayer et émonder"

### La technique de Carlson et al.
