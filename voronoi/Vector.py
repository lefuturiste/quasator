from math import sqrt
from typing import Any, Mapping, Optional, Sequence, Tuple, Union

DIMENSION = 2


class Vector:
    coordinates = []

    def __init__(self, coordinates):
        self.coordinates = coordinates

    @staticmethod
    def zero():
        return Vector(DIMENSION*[0])

    def canonical_dot_product(self, target):
        l = [
            self.coordinates[i] * target.coordinates[i]
            for i in range(DIMENSION)
        ]
        return sum(l)

    def dot_product(self, target):
        return self.canonical_dot_product(target)

    def norm(self):
        return sqrt(self.dot_product(self))

    def is_null(self):
        return self.norm() == 0
    
    def normalize(self):
        return (1/self.norm())*self

    def distance_to(self, target):
        return (target - self).norm()

    def __add__(self, o):
        return Vector([
            self.coordinates[i] + o.coordinates[i]
            for i in range(DIMENSION)
        ])

    def __sub__(self, o):
        return Vector([
            self.coordinates[i] - o.coordinates[i]
            for i in range(DIMENSION)
        ])

    def __mul__(self, o: Union[float, int]):
        if type(o) is int or type(o) is float:
            return Vector([o*self.coordinates[i] for i in range(DIMENSION)])
        else:
            raise ValueError(
                f'Cannot multiply a vector with unsuported type {type(o)}')

    __rmul__ = __mul__

    def __getitem__(self, item: int):
        if type(item) is not int:
            raise ValueError(
                f'Cannot get item in Vector, unsuported type {type(item)}')
        return self.coordinates[item]

    def __str__(self) -> str:
        j = ', '.join([str(c) for c in self.coordinates])
        return f'({j})'

    def __repr__(self):
        return f'Vector{str(self)}'

    def to_tuple(self):
        return tuple(self.coordinates)

# v2 = Vector([-1, -1])
# d = v1.distance_to(v2)
# print(d)

# v1 = Vector([1, 1])
# v2 = Vector([0, 0])

# d = v2.distance_to(v1)
# print(d)

# v1 = Vector([0, 2])
# v2 = Vector([-1, -1,])
# d = v1.distance_to(v2)
# print(d)
