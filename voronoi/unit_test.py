import unittest
from Vector import Vector
from Edge import Edge
from Voronoi import Voronoi
from math import sqrt


class TestVector(unittest.TestCase):

    def test_split(self):
        s = 'hello world'
        self.assertEqual(s.split(), ['hello', 'world'])
        with self.assertRaises(TypeError):
            s.split(2)

    def test_construct(self):
        self.assertEqual(Vector.zero().coordinates, [0, 0])
        self.assertEqual(Vector([100, 20]).coordinates, [100, 20])

        x, y = Vector([42, 43])
        self.assertEqual(x, 42)
        self.assertEqual(y, 43)

        self.assertEqual('Vector(1, 1)', repr(Vector([1, 1])))
        self.assertEqual((1, 1), Vector([1, 1]).to_tuple())
      
    def test_normalize(self):
        v1 = Vector([2, 2])
        v2 = Vector([0.7071067811865476, 0.7071067811865476])
        self.assertLess(v1.normalize().distance_to(v2), 0.0001)
        
        v1 = Vector([3.5, -1])
        v2 = Vector([0.9615239476408, -0.2747211278974])
        self.assertLess(v1.normalize().distance_to(v2), 0.0001)

    def test_distance(self):
        self.assertEqual(Vector([1, 1]).norm(), sqrt(2))
        self.assertEqual(Vector([2, 2]).norm(), 2*sqrt(2))
        self.assertEqual(Vector([0, 0]).norm(), 0)
        self.assertEqual(Vector([1, 1]).distance_to(Vector([0, 1])), 1)
        self.assertAlmostEqual(
            Vector([-1, -1]).distance_to(Vector([0, 2])), 3.16227766016)

    def test_operations(self):
        self.assertEqual((2*Vector([1, 1])).coordinates, [2, 2])
        self.assertEqual((0*Vector([1, 1])).coordinates, [0, 0])
        self.assertEqual((Vector([1, 1])+Vector([0, 2])).coordinates, [1, 3])
        self.assertEqual((Vector([1, 1])-Vector([0, 3])).coordinates, [1, -2])
        self.assertTrue(Vector([0, 0]).is_null())


class TestEdge(unittest.TestCase):
    def test_construct(self):
        self.assertEqual(Edge(Vector.zero(), Vector.zero()).length(), 0)

        e1 = Edge.from_coordinates((0, 1), (0, 2))
        self.assertEqual('Edge((0, 1), (0, 2))', repr(e1))
        s, e = e1
        self.assertEqual((e1.start, e1.end), (s, e))
        self.assertEqual(e1.length(), 1)

        e2 = Edge.from_coordinates((-1, -1), (2, 2))
        self.assertTrue(e2.contains(Vector([0.5, 0.5])))
        self.assertFalse(e2.contains(Vector([0, 1])))

        e3 = Edge.from_coordinates((1, 1), (2, 4))
        self.assertEqual(e3.get_middle().coordinates, [1.5, 2.5])

    def test_contains(self):
        # test for vertical line
        e1 = Edge.from_coordinates((1, 0), (1, 1))
        self.assertTrue(e1.contains(Vector([1, 0.5])))

        # test for weird floating point arithmetics issues
        e = Edge.from_coordinates(
            (0.5305555555555557, 1), (0.3083333333333332, 0))
        self.assertTrue(e.contains(
            Vector([0.41590909090909084, 0.4840909090909091])))
        self.assertFalse(e.contains(
            Vector([0.42590909090909084, 0.4840909090909091])))

    def test_intersect(self):
        e4 = Edge.from_coordinates((0, 0), (1, 1))
        e5 = Edge.from_coordinates((0, 1), (1, 0))
        self.assertTrue(e5.intersect_with(e4))
        self.assertTrue(e4.intersect_with(e5))

        self.assertTrue(type(e4.intersect_with(e5)) is Vector)
        self.assertEqual(e4.intersect_with(e5).coordinates, [0.5, 0.5])

        # vertical line intersect with horizontal line
        e4 = Edge.from_coordinates((0.5, 0), (0.5, 1))
        e5 = Edge.from_coordinates((0, 0.5), (1, 0.5))
        self.assertTrue(e4.intersect_with(e5))
        self.assertTrue(e5.intersect_with(e4))

        self.assertEqual(e4.intersect_with(e5).coordinates, [0.5, 0.5])

        # test for floating point issues
        e1 = Edge.from_coordinates(
            (0, 0.6529999999999999), (1, 0.7529999999999999))
        e2 = Edge.from_coordinates(
            (0.29583333333333334, 1), (0.3291666666666667, 0))
        i = e1.intersect_with(e2)
        self.assertLess(
            Vector([0.3063787375415282, 0.6836378737541527]).distance_to(i), 0.001)
        self.assertTrue(i)

    def test_vertical(self):
        e = Edge.from_coordinates((0.2, 0.2), (0.2, 0.8))
        self.assertTrue(e.is_vertical())

        e = Edge.from_coordinates((0, 0), (0.5, 0.8))
        self.assertFalse(e.is_vertical())

    def test_horizontal(self):
        e = Edge.from_coordinates((0.2, 0.2), (0.2, 0.8))
        self.assertFalse(e.is_horizontal())

        e = Edge.from_coordinates((0, 1.23), (0.5, 1.23))
        self.assertTrue(e.is_horizontal())

    def test_bounding_edge(self):
        bb = Edge.from_coordinates((0, 0), (1, 1))

        # inside the bounding box
        e = Edge.from_coordinates((0.2, 0.2), (0.6, 0.8))
        be = e.get_bounding_edge(bb)
        self.assertLess(Vector([0.06666, 0]).distance_to(be.start), 0.001)
        self.assertLess(Vector([0.7333, 1]).distance_to(be.end), 0.001)

        # outside the bounding box
        e = Edge.from_coordinates((0.2, 1.4), (0.4, 1.2))
        be = e.get_bounding_edge(bb)
        self.assertLess(Vector([0.6, 1]).distance_to(be.start), 0.001)
        self.assertLess(Vector([1, 0.6]).distance_to(be.end), 0.001)

        # intermediate
        e = Edge.from_coordinates((1.4, 0.8), (0.8, 0.6))
        be = e.get_bounding_edge(bb)
        self.assertLess(Vector([1, 0.6666]).distance_to(be.start), 0.001)
        self.assertLess(Vector([0, 0.333]).distance_to(be.end), 0.001)

        # vertical
        e = Edge.from_coordinates((0.2, 0.2), (0.2, 0.8))
        be = e.get_bounding_edge(bb, False)
        self.assertLess(Vector([0.2, 0]).distance_to(be.start), 0.001)
        self.assertLess(Vector([0.2, 1]).distance_to(be.end), 0.001)

        e = Edge.from_coordinates((0.2, 0.2), (0.2, 0.8))
        be = e.get_bounding_edge(bb, True)
        self.assertLess(Vector([0.2, 0.2]).distance_to(be.start), 0.001)
        self.assertLess(Vector([0.2, 1]).distance_to(be.end), 0.001)

        # horizontal
        e = Edge.from_coordinates((0.5, 0.5), (2, 0.5))
        be = e.get_bounding_edge(bb, False)
        self.assertLess(Vector([0, 0.5]).distance_to(be.start), 0.001)
        self.assertLess(Vector([1, 0.5]).distance_to(be.end), 0.001)

        e = Edge.from_coordinates((0.5, 0.5), (2, 0.5))
        be = e.get_bounding_edge(bb, True)
        self.assertLess(Vector([0.5, 0.5]).distance_to(be.start), 0.001)
        self.assertLess(Vector([1, 0.5]).distance_to(be.end), 0.001)

        e = Edge.from_coordinates((2, 0.5), (0.5, 0.5))
        be = e.get_bounding_edge(bb, True)
        self.assertLess(Vector([1, 0.5]).distance_to(be.start), 0.001)
        self.assertLess(Vector([0, 0.5]).distance_to(be.end), 0.001)

        e = Edge.from_coordinates((2, 0.5), (0.5, 0.5))
        be = e.get_bounding_edge(bb, False)
        self.assertLess(Vector([1, 0.5]).distance_to(be.end), 0.001)
        self.assertLess(Vector([0, 0.5]).distance_to(be.start), 0.001)

        # rectangle-shaped bounding box with ray
        bb = Edge.from_coordinates((0, 0), (2, 1))
        e = Edge.from_coordinates((0.2, 0.2), (1.2, 0.8))
        be = e.get_bounding_edge(bb, True)
        self.assertLess(Vector([0.2, 0.2]).distance_to(be.start), 0.001)
        self.assertLess(Vector([1.5333, 1]).distance_to(be.end), 0.001)
        # working!

    def test_median(self):
        bb = Edge.from_coordinates((0, 0), (2, 1))

        e = Edge.from_coordinates((0.2, 0.2), (0.6, 0.7))
        median = e.get_median()
        self.assertLess(Vector([0, 0.77777]).distance_to(median.start), 0.01)
        self.assertLess(Vector([1, -0.030]).distance_to(median.end), 0.01)
        be = median.get_bounding_edge(bb)
        self.assertLess(Vector([0.9625, 0]).distance_to(be.end), 0.001)

        # get median of a vertical line
        e = Edge.from_coordinates((0.5, 0.7), (0.5, 0.2))
        be = e.get_median().get_bounding_edge(bb)

        self.assertLess(Vector([0, 0.45]).distance_to(be.start), 0.001)
        self.assertLess(Vector([2, 0.45]).distance_to(be.end), 0.001)

        # get median of a horizontal line
        e = Edge.from_coordinates((0, 0.5), (1, 0.5))
        be = e.get_median().get_bounding_edge(bb)
        self.assertLess(Vector([0.5, 0]).distance_to(be.start), 0.001)
        self.assertLess(Vector([0.5, 1]).distance_to(be.end), 0.001)


class TestVoronoi(unittest.TestCase):

    def test_double(self):
        standard_bb = Edge.from_coordinates((0, 0), (1, 1))

        diag = Voronoi()
        points = [(0.2, 0.5), (0.8, 0.5)]
        diag.target_sites = [Vector(list(c)) for c in points]
        diag.bounding_box = standard_bb
        diag.generate_diagram()

        self.assertEqual(len(diag.edges), 1)
        self.assertEqual(diag.edges[0].to_tuple(), ((0.5, 0), (0.5, 1)))

    def test_triple(self):
        cases = [
            {  # OK
                'points': [
                    (0.25, 0.25),
                    (0.5, 0.5),
                    (0.75, 0.5),
                ],
                'segments': [
                    [(0.625, 0.125), (0, 0.75)],
                    [(0.625, 0.125), (0.625, 1)],
                    [(0.625, 0.125), (0.6875, 0)]
                ]
            },
            {  # OK
                'points': [
                    (0.1, 0.25),
                    (0.5, 0.5),
                    (0.75, 0.4),
                ],
                'segments': [
                    [(0.47987, 0.08719), (0, 0.85500)],
                    [(0.47987, 0.08719), (0.845, 1)],
                    [(0.47987, 0.08719), (0.5, 0)]
                ]
            },
            {  # OK
                'points': [
                    (0.15, 0.87),
                    (0.45, 0.88),
                    (0.19, 0.47)
                ],
                'segments': [
                    [
                        [0.3063787375415282, 0.6836378737541527],
                        [0.29583333333333334, 1]
                    ],
                    [
                        [0.3063787375415282, 0.6836378737541527],
                        [0, 0.6529999999999999]
                    ],
                    [
                        [0.3063787375415282, 0.6836378737541527],
                        [1, 0.24378048780488493]
                    ]
                ]
            },
            {
                'points': [
                    (0.82, 0.91),
                    (0.19, 0.31),
                    (0.81, 0.73)
                ],
                'segments': [
                    [
                        [0.27650837988826854, 0.8499162011173181],
                        [0.13357142857142842, 1]
                    ],
                    [
                        [0.27650837988826854, 0.8499162011173181],
                        [1, 0.8097222222222225]
                    ],
                    [
                        [0.27650837988826854, 0.8499162011173181],
                        [0.8522580645161287, 0]
                    ]
                ]
            }
        ]
        for case in cases:
          diag = Voronoi()
          diag.target_sites = [Vector(list(c)) for c in case['points']]
          diag.bounding_box = Edge.from_coordinates((0, 0), (1, 1))
          diag.generate_diagram()

          self.assertEqual(len(case['segments']), len(diag.edges))
          for i, s in enumerate(diag.edges):
              e = case['segments'][i]
              self.assertAlmostEqual(s[0][0], e[0][0], 4)
              self.assertAlmostEqual(s[0][1], e[0][1], 4)
              self.assertAlmostEqual(s[1][0], e[1][0], 4)
              self.assertAlmostEqual(s[1][1], e[1][1], 4)


if __name__ == '__main__':
    unittest.main()
