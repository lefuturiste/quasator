import numpy as np
from matplotlib import pyplot as plt
from time import sleep
from voronoi import voronoi

def main():
    plt.axis('scaled')
    plt.xlim(0, 1)
    plt.ylim(0, 1)

    plt.ion()
    plt.show()
    
    rand_f = lambda: round(np.random.uniform(), 2)

    for i in range(88):
        points = [
            (rand_f(), rand_f()) for i in range(3)
        ]

        print(f'{points=}')
        for p in points:
            plt.plot([p[0]], [p[1]], '.')

        segs = voronoi(points, False)
        colors = ['r', 'g', 'b', 'y', 'k']
        for i,s in enumerate(segs):
            a, b = s
            plt.plot([a[0], b[0]], [a[1], b[1]], colors[i%len(colors)])
        
        plt.pause(0.001)
        input("Press [enter] to continue.")

if __name__ == '__main__':
    main()