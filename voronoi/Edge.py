from Vector import Vector

FP_PRECISION = 1e-10

class Edge:

    def __init__(self, start, end):
        self.start = start
        self.end = end

    # construct an edge object with a and b being list of coordinates
    @staticmethod
    def from_coordinates(a, b):
        return Edge(Vector(list(a)), Vector(list(b)))

    @staticmethod
    def from_parameters(slope, offset):
        def f(x): return slope*x+offset
        return Edge.from_coordinates((0, f(0)), (1, f(1)))

    def is_vertical(self):
        return (self.start[0]-self.end[0]) == 0

    def is_horizontal(self):
        return (self.start[1]-self.end[1]) == 0

    def length(self):
        return self.start.distance_to(self.end)

    def __getitem__(self, item: int):
        if item == 0:
            return self.start
        if item == 1:
            return self.end
        return [][item]

    def __str__(self) -> str:
        return f'({str(self.start)}, {str(self.end)})'

    def __repr__(self):
        return f'Edge{str(self)}'

    def to_tuple(self):
        return (self.start.to_tuple(), self.end.to_tuple())

    """
  Get the slope of the line formed by the Edge
  return None if slope if it's a vertical line
  """

    def get_slope(self):
        if self.is_vertical():
            return None
        return (self[1][1]-self[0][1])/(self[1][0]-self[0][0])

    def get_slope_x(self):
        return self[1][0]-self[0][0]

    def get_slope_y(self):
        return self[1][1]-self[0][1]

    def get_vector(self) -> Vector:
        return Vector([
            self.get_slope_x(),
            self.get_slope_y()
        ])

    def get_offset(self):
        if self.is_vertical():
            return 0
        return self[0][1] - self.get_slope()*self[0][0]

    # evaluate y if it was an infinite line
    def evaluate(self, x):
        return x*self.get_slope()+self.get_offset()

    # get middle point of this edge
    def get_mean(self) -> Vector:
        return 0.5*(self.start+self.end)

    get_middle = get_mean

    def reverse(self):
        dx = self.end[0]-self.start[0]
        dy = self.end[1]-self.start[1]
        return Edge(
            self.start,
            Vector([
                self.start[0] - dx,
                self.start[1] - dy
            ])
        )

    # check if the point is in a given edge
    # given that the point is on the infinite line defined by the edge
    def contains(self, p: Vector) -> bool:
        if self.is_vertical():
            # check if it's on the same x axis
            if abs(self.start[0] - p[0]) > FP_PRECISION:
                return False
        e1 = Edge(self.start, p)
        #e2 = Edge(self.end, p)
        if e1.is_horizontal() != self.is_horizontal():
            return False

        if (not e1.is_vertical()) and abs(e1.get_slope() - self.get_slope()) > FP_PRECISION:
            return False
        # if (not e2.is_vertical()) and abs(e2.get_slope() - self.get_slope()) > 1e-13:
        # 	return False
        d = self.length()  # distance the two points that define a seg
        d1 = p.distance_to(self.start)
        d2 = p.distance_to(self.end)
        return d1 <= d and d2 <= d

    def intersect_with(self, t):
        # False if no intersection, point cordinate otherwise
        # a and b are tuple of points that represent each a segment
        i = (0, 0)
        if (
            self.is_vertical() and t.is_vertical()
            and abs(self[0][0] - t[0][0]) > FP_PRECISION  # float are different
        ):
            return False

        if self.is_vertical():
            i = [self.start[0], t.evaluate(self.start[0])]
        elif t.is_vertical():
            i = [t.start[0], self.evaluate(self.end[0])]
        else:
            s1, o1 = self.get_slope(), self.get_offset()
            s2, o2 = t.get_slope(), t.get_offset()

            if abs(s1-s2) < FP_PRECISION:  # the lines are parallels
                return False

            x = (o2-o1)/(s1-s2)
            i = [x, x*s1+o1]
        intersection = Vector(i)
        if self.contains(intersection) and t.contains(intersection):
            return intersection

        return False

    # get the extended or reduced edge based on the current edge
    # given a bounding box
    # bounding box format: ((x_start, y_start), (x_end, y_end)) -> an Edge
    # default bouding box: ((0,0), (1,1))
    # return False if the edge is out of bound
    # if the ray parameter is True we will only bound in the direction of the end vertex
    def get_bounding_edge(self, bounding_box, ray=False):
        bx_min, by_min = bounding_box[0]  # the start
        bx_max, by_max = bounding_box[1]  # the end
        if self.is_vertical():
            x = self[0][0]
            if not (bx_min <= x <= bx_max):
                return False
            if ray:
                if self[1][1] > self[0][1]:  # the end vertex is above the start
                    return Edge.from_coordinates(
                        (x, self[0][1]), (x, by_max)
                    )
                if self[1][1] < self[0][1]:  # the end vertex is bellow the start
                    return Edge.from_coordinates(
                        (x, self[0][1]), (x, by_min)
                    )
            return Edge.from_coordinates(
                (x, by_min), (x, by_max)
            )
        # check if it's bonding
        slope, offset = self.get_slope(), self.get_offset()

        if self.is_horizontal():
            y = self[0][1]
            x_start = self[0][0]

            if not (by_min <= y <= by_max):
                return False

            if x_start > bx_max:
                x_start = bx_max
            if x_start < bx_min:
                x_start = bx_min

            if ray:
                if self[1][0] > self[0][0]:  # the end vertex is at the right of the start
                    return Edge.from_coordinates(
                        (x_start, y), (bx_max, y)
                    )
                if self[1][0] < self[0][0]:  # the end vertex is at the left of the start
                    return Edge.from_coordinates(
                        (x_start, y), (bx_min, y)
                    )
            return Edge.from_coordinates((bx_min, y), (bx_max, y))

        # solve for top and bottom x
        bottom = (by_min - offset)/slope
        if bx_min <= bottom <= bx_max:
            o1 = (bottom, by_min)
        elif bottom > bx_max:
            o1 = (bx_max, self.evaluate(bx_max))
        elif bottom < bx_min:
            o1 = (bx_min, self.evaluate(bx_min))

        top = (by_max - offset)/slope
        if bx_min <= top <= bx_max:
            o2 = (top, by_max)
        elif top > bx_max:
            o2 = (bx_max, self.evaluate(bx_max))
        elif top < bx_min:
            o2 = (bx_min, self.evaluate(bx_min))

        o1, o2 = Vector(o1), Vector(o2)

        if ray:
            if self.end[0] > self.start[0]:
                # the exit point will be at the right of the origin
                if o1[0] > self.start[0]:
                    return Edge(self.start, o1)
                if o2[0] > self.start[0]:
                    return Edge(self.start, o2)
            if self.end[0] < self.start[0]:
                # the exit point will be at the left of the origin
                if o1[0] < self.start[0]:
                    return Edge(self.start, o1)
                if o2[0] < self.start[0]:
                    return Edge(self.start, o2)
        if not ray:
            if self.end[0] > self.start[0]:
                # the exit point will be at the right of the origin
                if o1[0] > self.start[0]:
                    return Edge(o2, o1)
                if o2[0] > self.start[0]:
                    return Edge(o1, o2)
            if self.end[0] < self.start[0]:
                # the exit point will be at the left of the origin
                if o1[0] < self.start[0]:
                    return Edge(o2, o1)
                if o2[0] < self.start[0]:
                    return Edge(o1, o2)

        return False

    def get_median(self):
        m = self.get_mean()
        if self.is_vertical():
            return Edge.from_coordinates(m, (m[0]+1, m[1]))

        if self.is_horizontal():
            return Edge.from_coordinates(m, (m[0], m[1]+1))

        slope = -1/self.get_slope()
        offset = m[1]-slope*m[0]

        return Edge.from_parameters(slope, offset)

    # get the median segment of it self, given a bounding box
    # def get_median(self, bounding_box):
    # 	if self.is_vertical():
    # 		return (0, ym), (1, ym)

    # 	mean = self.get_mean()
    # 	xm, ym = get_mean(p1, p2)

    # 	slope = (y2-y1)/(x2-x1)
    # 	o1, o2 = (0, 0), (0, 0)

    # 	# function of the median line
    # 	g = lambda x: (-x/slope)+ym+xm/slope

    # 	# solve for top and bottom x
    # 	bottom = xm+slope*ym
    # 	print('bottom', bottom)
    # 	if 0 <= bottom <= 1:
    # 		o1 = (bottom, 0)
    # 	elif bottom > 1:
    # 		o1 = (1, g(1))
    # 	elif bottom < 0:
    # 		o1 = (0, g(0))

    # 	top = -slope+ym*slope+xm
    # 	print('top', top)
    # 	if 0 <= top <= 1:
    # 		o2 = (top, 1)
    # 	elif top > 1:
    # 		o2 = (1, g(1))
    # 	elif top < 0:
    # 		o2 = (0, g(0))

    # 	print('o1', o1)
    # 	print('o2', o2)

    # 	if plot:
    # 		plt.plot([xm], [ym], '.g')

    # 	return o1,o2

# e = Edge(Vector.zero(), Vector([1, 1]))

# print(e.length())
# print(e.contains(Vector([0.5, 0.5])))
# print(e.intersect_with(Edge(Vector([0, -1]), Vector([1, 0]))))
