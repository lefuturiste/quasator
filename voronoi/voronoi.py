import numpy as np
import matplotlib.pyplot as plt

# the bounding box is (0, 1) (1, 1)
plt.axis('scaled')
plt.xlim(0, 1)
plt.ylim(0, 1)

def voronoi(points, plot = False):

	NB_POLES = 2
	poles = []
	segments = []
	segments_binding = [] # bind a segment with a medium line

	# get mean of two points
	def get_mean(p1, p2):
		x1, y1 = p1
		x2, y2 = p2
		return ((x1+x2)/2, (y1+y2)/2)

	# get the median
	# return a Line object (two points)
	def get_median(p1, p2):
		x1, y1 = p1
		x2, y2 = p2
		xm, ym = get_mean(p1, p2)
		if (x2-x1) == 0:
			# line between p1 and p2 is a vertical line
			# so the median will me an horizontal line
			return (0, ym), (1, ym)

		slope = (y2-y1)/(x2-x1)
		o1, o2 = (0, 0), (0, 0)

		# function of the median line
		g = lambda x: (-x/slope)+ym+xm/slope

		# solve for top and bottom x
		bottom = xm+slope*ym
		print('bottom', bottom)
		if 0 <= bottom <= 1:
			o1 = (bottom, 0)
		elif bottom > 1:
			o1 = (1, g(1))
		elif bottom < 0:
			o1 = (0, g(0))

		top = -slope+ym*slope+xm
		print('top', top)
		if 0 <= top <= 1:
			o2 = (top, 1)
		elif top > 1:
			o2 = (1, g(1))
		elif top < 0:
			o2 = (0, g(0))

		print('o1', o1)
		print('o2', o2)

		if plot:
			plt.plot([xm], [ym], '.g')

		return o1,o2


	def get_slope(segment):
		p1, p2 = segment
		x1, y1 = p1
		x2, y2 = p2
		return (y2-y1)/(x2-x1)

	def get_offset(segment):
		s = get_slope(segment)
		return segment[0][1] - s*segment[0][0]

	def is_vertical(segment):
		p1, p2 = segment
		x1, y1 = p1
		x2, y2 = p2
		return (x2-x1) == 0

	def dist(a, b):
		# get the distance between two points
		return np.hypot(a[0]-b[0], a[1]-b[1])

	def in_segment(m, seg):
		# check if the point is in a given segment
		# we suppose that the point is on the infinite line defined by the segment
		a, b = seg
		d = dist(a, b) # distance the two points that define a seg
		d1 = dist(m, a) # distance between m and a
		d2 = dist(m, b) # distance between m and b

		return d1 <= d and d2 <= d

	def intersect(a, b):
		# False if no intersection, point cordinate otherwise
		# a and b are tuple of points that represent each a segment
		i = (0, 0)
		if is_vertical(a) and is_vertical(b) and a[0][0] != b[0][0]:
			return False
		if is_vertical(a):
			s2, o2 = get_slope(b), get_offset(b)
			i = (a[0][0], s2*a[0][0]+o2)
		elif is_vertical(b):
			s1, o1 = get_slope(a), get_offset(a)
			i = (b[0][0], s1*b[0][0]+o1)
		else:
			s1, o1 = get_slope(a), get_offset(a)
			s2, o2 = get_slope(b), get_offset(b)

			x = (o2-o1)/(s1-s2)
			i = (x, x*s1+o1)

		if in_segment(i, a) and in_segment(i, b):
			return i
		return False

	def closest(p):
		# get the closest pole from a given point
		px, py = p
		dists = [
			(i, np.hypot(ppx-px, ppy-py))
			for i, (ppx, ppy) in enumerate(poles)
		]
		dists.sort(key=lambda e: e[1])
		return [poles[i] for i,p in dists]

	# return true if a segment can be drawn between a and b without intersection
	def are_directly_connected(a, b):
		s = (a, b)
		for seg in segments:
			if intersect(s, seg):
				return False
		return True

	# get abs angle of a segment
	def angle_seg(s):
		a, b = s
		return np.arctan2(b[1]-a[1], b[0]-a[0])


	def get_ray(a, b, revert = False):
		# ray: demi droite en bon français
		# from two points that define a ray
		# a being the origin and b indicating the direction
		# we return (a, i) i being the intersection off screen
		if revert:
			# rotate b 180° from a
			b = (a[0]-(b[0]-a[0]), a[1]-(b[1]-a[1]))		
		
		x1, y1 = a
		x2, y2 = b

		if is_vertical((a,b)):
			if y2 > y1: # the direction is above the origin
				return (a, (x1, 1))
			if y2 < y1: # the direction is above the origin
				return (a, (x1, 0))
			
			raise ValueError(f'Cannot compute vertical ray for {a=} {b=}')

		slope = get_slope((a, b))
		offset = get_offset((a, b))
		f = lambda x: slope*x+offset

		# solve for top and bottom x
		bottom = -offset/slope
		if 0 <= bottom <= 1:
			o1 = (bottom, 0)
		elif bottom > 1:
			o1 = (1, f(1))
		elif bottom < 0:
			o1 = (0, f(0))

		top = (1-offset)/slope
		if 0 <= top <= 1:
			o2 = (top, 1)
		elif top > 1:
			o2 = (1, f(1))
		elif top < 0:
			o2 = (0, f(0))

		if x2 > x1:
			# the exit point will be at the right of the origin
			if o1[0] > x1: return (a, o1)
			if o2[0] > x1: return (a, o2)
		if x2 < x1:
			# the exit point will be at the left of the origin
			if o1[0] < x1: return (a, o1)
			if o2[0] < x1: return (a, o2)

		raise ValueError(f'Cannot compute ray for {a=} {b=}')

	def add_point(p):
		px, py = p
		# get the closest point
		# draw a medium line between the new pole and the closest
		# compute each intersection with all others lines
		# we have to keep in memories all the lines equations
		# for each intersection
			# draw a medium line between the new pole and the pole which line collide with our line
			# do that until no further collisions
		if plot:
			plt.text(px,py,'P' + str(len(poles)))
			plt.plot([px], [py], '.y')

		c1 = closest(p)[0]
		m1 = get_mean(p, c1)
		o1, o2 = get_median(p, c1)

		has_intersect = False
		for s_id, seg in enumerate(segments):
			i = intersect((o1, o2), seg)
			if i:
				has_intersect = True
				ix, iy = i
				# get the point associated by the segment
				c2 = list(filter(lambda c: c != p and c != c1, closest(i)))[0]
				print('second closest', c2)
				m2 = get_mean(p, c2)
				oo1, oo2 = get_median(p, c2)

				#plt.plot([ix], [iy], '.')

		if has_intersect:
			#segments.append((o1, i))
			old_segment = segments.pop()
			direction = [ m for s,m in segments_binding if s == old_segment ][0]
			poles.append(p)
			print('new pole', p)
			#print('dir', direction, 'closest from dir', tmp)
			# if intersect(old):
			# 	dx, dy = direction
			# 	direction = (i[0] - (dx-i[0]), i[1] - (dy-i[1]))
			print(f'{direction=}')

			r = get_ray(i, direction)
			if (dist(direction, p) < dist(direction, points[0])) and (dist(direction, p) < dist(direction, points[1])):
					print('RAY REVERSED')
					r = get_ray(i, m2, True)

			segments.append(r)
			segments.append(get_ray(
				i, m1,
				#not intersect((i, m1), (p, c1))
			))
			
			#a1, a2, a3 = angle_seg((i, direction)), angle_seg(r), angle_seg((i, m1))

			# check if the reversed ray is going to be
			#if (a1 < a2 < a3):
			#	print('RAY REVERSED')
			#	r = get_ray(i, m2, True)
			# if P0 and P2 are directly connected

			# we need a way to erase properly the rest of the ray
			# we trace both rays
			# for each choice:
			# we take a point on that choice
			# if it's closer to P0 and P1 than P2
			# we remove the parts of the edges that are closer to a third site than the two sites in its name
			# the two sites in its name are: P0 and P2
			# if 
			dp = m2 # direction prototype
			site = c2
			r = get_ray(i, m2)

			print(f'{i=} {m1=} {m2=}')
			print(f'{dp=} {c1=} {c2=}')
			if (dist(dp, c1) < dist(dp, p)) and (dist(dp, c1) < dist(dp, c2)):
					print('RAY REVERSED')
					r = get_ray(i, m2, True)

			segments.append(r)
			#segments.append((i, oo2))
		if not has_intersect:
			segments.append(
				get_median(p, closest(p)[0])
			)

	for i in range(0, 2):
		px, py = points[i]
		if plot:
			plt.text(px,py,'P' + str(i))
		poles.append((px, py))

	if plot:
		plt.plot([x for x,y in poles], [y for x,y in poles], '.')

	o1, o2 = get_median(poles[0], poles[1])
	segments.append((o1, o2))
	segments_binding.append(((o1, o2), get_mean(poles[0], poles[1])))

	# (np.random.uniform(), np.random.uniform())
	add_point(points[2])

	if plot:
		colors = ['r', 'g', 'b', 'y', 'k']
		for i,s in enumerate(segments):
			a, b = s
			plt.plot([a[0], b[0]], [a[1], b[1]], colors[i%len(colors)])
	return segments


rand_f = lambda: round(np.random.uniform(), 2)

def rand_draw():
	points = [
		(rand_f(), rand_f()) for i in range(3)
	]
	print(f'{points=}')
	segs = voronoi(points, True)
	plt.draw()


# TODO: CHECK IF A CELL IS EMPTY

#segs = voronoi(points, True)

# import json
# f = open('./voronoi_out.json', 'w')
# f.write(json.dumps({'points': points,'segs': segs}, indent=2))
# f.close()

def assert_algo():
	test_cases = [
		{
			'points': [
				(0.25, 0.25),
				(0.5, 0.5),
				(0.75, 0.5),
			],
			'segments': [
				[(0.625, 0.125), (0, 0.75)],
				[(0.625, 0.125), (0.625, 1)],
				[(0.625, 0.125), (0.6875, 0)]
			]
		},
		{
			'points': [
				(0.1, 0.25),
				(0.5, 0.5),
				(0.75, 0.4),
			],
			'segments': [
				[(0.47987, 0.08719), (0, 0.85500)],
				[(0.47987, 0.08719), (0.845, 1)],
				[(0.47987, 0.08719), (0.5, 0)]
			]
		},
		{
			'points': [
	      (0.15, 0.87),
	      (0.45, 0.88),
	      (0.19, 0.47)
	    ],
	    'segments': [
		    [
		      [0.3063787375415282,0.6836378737541527],
		      [0.29583333333333334,1]
		    ],
		    [
		      [0.3063787375415282,0.6836378737541527],
		      [0,0.6529999999999999]
		    ],
		    [
		      [0.3063787375415282,0.6836378737541527],
		      [1,0.24378048780488493]
		    ]
	    ]
		},
		{
			'points': [
				(0.82, 0.91),
		    (0.19, 0.31),
		    (0.81, 0.73)
			],
			'segments': [
				[
		      [0.27650837988826854, 0.8499162011173181],
		      [0.17483870967741955, 1]
		    ],
		    [
		      [0.27650837988826854, 0.8499162011173181],
		      [1, 0.8097222222222225]
		    ],
		    [
		      [0.27650837988826854, 0.8499162011173181],
		      [0.8522580645161287, 0]
		    ]
			]
		}
	]

	def assert_number(n1, n2, d = 0.001):
		if abs(n1-n2) < d:
			return True
		raise AssertionError(f'Expected {n1} got {n2} at distance {d}')

	for case in test_cases:
		if case['segments'] == []: continue
		segs = voronoi(case['points'])
		for i, s in enumerate(segs):
			e = case['segments'][i]
			assert_number(e[0][0], s[0][0])
			assert_number(e[0][1], s[0][1])
			assert_number(e[1][0], s[1][0])
			assert_number(e[1][1], s[1][1])


if __name__ == '__main__':
    assert_algo()