from Voronoi import Voronoi
from Vector import Vector
from Edge import Edge

import numpy as np
import matplotlib.pyplot as plt

plt.axis('scaled')
plt.xlim(0, 1)
plt.ylim(0, 1)

def assert_algo():
    test_cases = [
        {  # OK
            'points': [
                (0.25, 0.25),
                (0.5, 0.5),
                (0.75, 0.5),
            ],
            'segments': [
                [(0.625, 0.125), (0, 0.75)],
                [(0.625, 0.125), (0.625, 1)],
                [(0.625, 0.125), (0.6875, 0)]
            ]
        },
        {  # OK
            'points': [
                (0.1, 0.25),
                (0.5, 0.5),
                (0.75, 0.4),
            ],
            'segments': [
                [(0.47987, 0.08719), (0, 0.85500)],
                [(0.47987, 0.08719), (0.845, 1)],
                [(0.47987, 0.08719), (0.5, 0)]
            ]
        },
        {  # OK
            'points': [
                (0.15, 0.87),
                (0.45, 0.88),
                (0.19, 0.47)
            ],
            'segments': [
                [
                        [0.3063787375415282, 0.6836378737541527],
                        [0.29583333333333334, 1]
                ],
                [
                    [0.3063787375415282, 0.6836378737541527],
                    [0, 0.6529999999999999]
                ],
                [
                    [0.3063787375415282, 0.6836378737541527],
                    [1, 0.24378048780488493]
                ]
            ]
        },
        {
            'points': [
                (0.82, 0.91),
                (0.19, 0.31),
                (0.81, 0.73)
            ],
            'segments': [
                [
                        [0.27650837988826854, 0.8499162011173181],
                        [0.13357142857142842, 1]
                ],
                [
                    [0.27650837988826854, 0.8499162011173181],
                    [1, 0.8097222222222225]
                ],
                [
                    [0.27650837988826854, 0.8499162011173181],
                    [0.8522580645161287, 0]
                ]
            ]
        }
    ]

    def assert_number(n1, n2, d=0.001):
        if abs(n1-n2) < d:
            return True
        raise AssertionError(f'Expected {n1} got {n2} at distance {d}')

    for i, case in enumerate(test_cases):
        print(f'CASE {i}')
        if case['segments'] == []:
            continue
        diag = Voronoi()
        diag.target_sites = [Vector(list(c)) for c in case['points']]
        diag.bounding_box = Edge.from_coordinates((0, 0), (1, 1))
        diag.generate_diagram()

        for i, s in enumerate(diag.edges):
            e = case['segments'][i]
            assert_number(e[0][0], s[0][0])
            assert_number(e[0][1], s[0][1])
            assert_number(e[1][0], s[1][0])
            assert_number(e[1][1], s[1][1])

    print('==> ASSERTION SUCCESS')


def main():
    points = [
        (0.82, 0.91),
        (0.19, 0.31),
        (0.81, 0.73),
        (0.40, 0.15),
        #(0.90, 0.17)
    ]

    diag = Voronoi()
    diag.plt = plt
    diag.target_sites = [Vector(list(c)) for c in points]
    diag.bounding_box = Edge.from_coordinates((0, 0), (1, 1))
    diag.generate_diagram()

    for i, site in enumerate(diag.target_sites):
        plt.plot([site[0]], [site[1]], '.')
        plt.text(site[0], site[1], f'P{i}')

    colors = ['r', 'g', 'b', 'y', 'k', 'm']
    for i, edge in enumerate(diag.edges):
        a, b = edge.to_tuple()
        plt.plot([a[0], b[0]], [a[1], b[1]], colors[i % len(colors)])

    print(diag.edges)
    plt.show()


if __name__ == '__main__':
    #assert_algo()
    main()
