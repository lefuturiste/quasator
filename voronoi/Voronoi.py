from ctypes import pointer
from Vector import Vector
from Edge import Edge


class Voronoi:

    """
    We want to define a good working a useful and pratical data structure

    vertex: a point with two coordinates
    list of edges: A list of tuple of vertex that represent all the edges
    an edge 
    list of sites: A list of tuple of coordinate
    list of cells
    """

    def __init__(self):
        self.edges = []
        self.edges_binding = []

        self.target_sites = []
        self.definitive_sites = []
        self.bounding_box = None

        # add point index
        self.api = 0

    def generate_diagram(self):
        self.sites_stack = list(reversed(self.target_sites.copy()))

        # generate with the first two points
        p1 = self.sites_stack.pop()
        p2 = self.sites_stack.pop()
        m = Edge(p1, p2).get_median()
        self.add_edge(
            m.get_bounding_edge(self.bounding_box),
            (p1, p2)
        )
        self.definitive_sites.append(p1)
        self.definitive_sites.append(p2)

        while len(self.sites_stack) > 0:
            self.add_point()

    # add an edge and bind it with a tuple of sites
    def add_edge(self, edge, sites):
        self.edges.append(edge)
        self.edges_binding.append(sites)

    def get_association_of_edge(self, edge):
        i = self.edges.index(edge)
        return self.edges_binding[i]

    def remove_edge(self, edge):
        i = self.edges.index(edge)
        self.edges.pop(i)
        self.edges_binding.pop(i)

    # get the closests points in the diagram from a particular point
    def get_closests(self, target):
        dists = [
            (i, site.distance_to(target))
            for i, site in enumerate(self.definitive_sites)
        ]
        # sort by the distance, closest first
        dists.sort(key=lambda e: e[1])
        return [self.definitive_sites[i] for i, d in dists]

    # the most important peice of code!
    def add_point(self):
        self.api += 1
        new_point = self.sites_stack.pop()
        print('========')
        print('ADD_POINT', new_point)

        # get the closest point
        closest = self.get_closests(new_point)[0]

        stack = [closest]
        visited = [closest]

        lines = []

        edges_to_remove = []
        while len(stack) > 0:
            current = stack.pop()
            print('=> unstacking', current)
            visited.append(current)

            # draw a median
            e_mean = Edge(new_point, current)
            mean = e_mean.get_mean()
            median = e_mean.get_median(
            ).get_bounding_edge(self.bounding_box)

            potentials_intersections = []
            for edge in self.edges:
                i = median.intersect_with(edge)
                print('intersect', median, edge, i)
                if i:
                    potentials_intersections.append((i, edge))
                    #if len(intersect_history) > 0:
                    #    # get coordinate with the basis (origin, intersection)
                    #    # if the dot product is negative then the vector are in opossite direction
                    #    #v1 = get vector from Edge(origin, last_intersect)
                    #    #v2 = get vector from Edge(origin, new_intersect)
                    #    v1 = Edge(mean, intersect_history[-1]).get_vector()
                    #    v2 = Edge(mean, i).get_vector()
                    #    print('checking for intersection discartion', v1, v2)
                    #    self.plt.plot([v1[0]], [v1[1]], '.k')
                    #    self.plt.plot([v2[0]], [v2[1]], '.m')
                    #    if (
                    #        (v1.dot_product(v2) > 0) and
                    #        v2.norm() > v1.norm()
                    #    ):
                    #        print('intersection discarded')
                    #        # we discard this intersection
                    #        continue

                    #    #
                    #    #if v1.dot(v2) < 0:
                    #    #    # then we check for distance
                    #    #    # if the norm of v2 is greater than v1 then discard
                    #    #    # if the norm of v2 is less than v1 then accept
                    #    
                        # check if this new intersection is above or bellow the current one
                    # if not has_intersect:
                    # 	# it's the first intersection
                    # 	# we now may only consider intersection ABOVE or BELLOW the current one

            if len(potentials_intersections) > 1:
                # now we have a list of potentials intersections
                # we must split in two parts all the intersections
                # intersections grouped by their direction
                # group 1: intersections that have dot product of > 1 with the mean
                v1 = Edge(mean, potentials_intersections[0][0]).get_vector()
                g1, g2 = [], []
                for i, e in potentials_intersections:
                    v2 = Edge(mean, i).get_vector()
                    if v2.dot_product(v1) > 0:
                        g1.append(((i, e), i.distance_to(mean)))
                    else:
                        g2.append(((i, e), i.distance_to(mean)))
                # now we have two groups,
                # for each group we will sort then by their distance to the mean
                # we take the closest each time
                g1.sort(key=lambda x: x[1])
                g2.sort(key=lambda x: x[1])
                intersections = [g1[0][0]]
                if len(g2) > 0: intersections.append(g2[0][0])
            else:
                intersections = potentials_intersections
            
            # now we have the list of all the intersections
            # it's either 0, 1 or 2 intersections
            for i, edge in intersections:
                # search what is the point associated with this intersection
                # match edge with the corresponding shit
                a1, a2 = self.get_association_of_edge(edge)
                # it's a2 that is the point associated with the edge or it's a1
                associated = a2 if a1 == current else a1
                # check if the point has already being visited
                print('associated', associated)
                if not (associated in visited):
                    stack.append(associated)
                    # delete old edge
                    edges_to_remove.append((a1, a2, edge))
                    lines.append((a1, a2, edge, [ i for i, e in intersections ]))

            print('intersections', intersections)
            # structure: 2 points, median of these two points, intersections
            lines.append((new_point, current, median, [ i for i, e in intersections ]))

        for a1, a2, e in edges_to_remove:
            print('REMOVED EDGE', e)
            #lines.append((a1, a2, e, intersections))
            self.remove_edge(e)

        print('lines after add point', lines)
        for p1, p2, l, ih in lines:
            print('== tracing line ==')
            print((p1, p2, l, ih))
            if len(ih) == 0:
                print('not tracing as a ray')
                final = l
            if len(ih) == 1:
                print('line traced as ray')
                direction = Edge(p1, p2).get_mean()
                final = Edge(ih[0], direction).get_bounding_edge(
                    self.bounding_box, True)
            if len(ih) == 2:
                print('line traced as a pure EDGE')
                final = Edge(ih[0], ih[1])

            if len(ih) == 1:
                # we remove the parts of the edges definitive_sites
                # that are closer to a third site
                # than the two sites in its name
                for ds in self.definitive_sites+[new_point]:
                    print('ray', ds, p1, p2)
                    if ds == p1 or ds == p2:
                        continue
                    print('ray checking', ds, direction.distance_to(
                        p1), direction.distance_to(ds))
                    if (
                            direction.distance_to(
                                ds) < direction.distance_to(p1)
                    ):
                        # remove because that part is closer to a third site
                        final = final.reverse().get_bounding_edge(self.bounding_box, True)
                        break
            print('final draw', final)
            self.add_edge(final, (p1, p2))
        self.definitive_sites.append(new_point)
