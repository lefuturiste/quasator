# QuaSATor

Detect collisions between satellite and debris in LEO (Low-Earth Orbit)

## Install

## Requirements

- mysql-connector-python

## Scripts

- 1. Fetch OMM (XML) LEO ELSET catalog
- 2. Migrate
- 3. 

## 2d_collisions

### sandbox the kd_tree

Run in the *quasator* directory:

`/m/d/w/quasator> python3 -m 2d_collisions.kd_tree.sandbox -i 2d_collisions/kd_tree/sandbox.py`

### run tests on kd_tree

`/m/d/w/quasator> python3 -m unittest 2d_collisions/kd_tree/test.py`
