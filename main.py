import pprint
from mysql.connector import connect, Error

def get_connection():
  return connect(
    host="localhost",
    user="root",
    password="root",
    database="quasator",
  )

with get_connection() as connection:
  with connection.cursor() as cursor:
    cursor.execute("SELECT * FROM user")
    for row in cursor.fetchall():
      pprint.pprint(row)
