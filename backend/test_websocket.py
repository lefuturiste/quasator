from src.WebSocketServer import WebSocketServer
from src.Container import Container
from src.Logger import LoggerManager
from time import sleep

container = Container()

logger = LoggerManager()
logger.setLevel('debug')
container.set('logger', logger)

websocket = WebSocketServer(container)
container.set('websocket', websocket)

try:
  logger.startClock()
  websocket.start()
  while True:
    sleep(10)
except KeyboardInterrupt:
  print('---KeyboardInterrupt')
  websocket.stop()