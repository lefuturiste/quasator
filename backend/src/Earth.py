from astronomy import Seasons
from datetime import datetime
from math import pi

def get_jun_solstice(year: int) -> float:
    return Seasons(year-1).jun_solstice.tt*86400

def compute_rotation_axis_cone_pos(epoch: float) -> float:
    """
    Compute the rotation axis cone position for the given POSIX epoch.
    """
    
    # get some general params for simulating the earth
    # depending on the date
    # we expect args.epoch to be a epoch in seconds
    # we convert to a J2000 epoch by substracting the unix epoch on Jan 1, 2000
    timestamp = datetime.utcfromtimestamp(epoch)
    epoch = epoch-946684800
    
    lastYearSolstice = get_jun_solstice(timestamp.year-1)
    currentYearSolstice = get_jun_solstice(timestamp.year)
    nextYearSolstice = get_jun_solstice(timestamp.year+1)
    # get the duration between the last summer solstice and the timestamp in seconds

    # we have three cases
    # we need to get:
    # 1. the number of seconds since the last jun solstice
    # 2. the number of seconds between the last jun solstice and the next dec solstice
    
    durationSinceLastJunSolstice = 0
    durationBetweenSolstices = 0

    if (epoch - currentYearSolstice) < 0:
        # we are at the start of the year
        durationSinceLastJunSolstice = abs(epoch - lastYearSolstice)
        durationBetweenSolstices = abs(currentYearSolstice - lastYearSolstice)
    
    if (epoch - currentYearSolstice) > 0:
        # we are at the end of the year
        durationSinceLastJunSolstice = epoch - currentYearSolstice
        durationBetweenSolstices = abs(currentYearSolstice - nextYearSolstice)
    
    rate = 2*pi/durationBetweenSolstices
    axisConePos = durationSinceLastJunSolstice*rate

    return axisConePos

if __name__ == '__main__':
    print(compute_rotation_axis_cone_pos(1650106136))
