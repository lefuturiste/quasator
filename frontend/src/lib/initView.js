import {
    AmbientLight,
    AxesHelper,
    BackSide,
    Color,
    CylinderGeometry,
    DirectionalLight,
    DirectionalLightHelper,
    Group,
    Matrix4,
    Mesh,
    MeshBasicMaterial,
    MeshPhongMaterial,
    PerspectiveCamera,
    Scene,
    SphereGeometry,
    TextureLoader,
    WebGLRenderer,
} from "three";
import { OrbitControls } from "./orbitControls";
import getAtmosphereMaterial from "./atmosphereMaterial";
import { createGroup, degToRad } from "./utils";

const initView = () => {
    const scene = new Scene();

    const renderer = new WebGLRenderer({ antialias: true });
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);

    document
        .getElementById("webgl-renderer-container")
        .appendChild(renderer.domElement);

    const camera = new PerspectiveCamera(
        40,
        window.innerWidth / window.innerHeight,
        1,
        1000
    );
    camera.position.set(15, 20, 100);
    scene.add(camera);

    // controls
    const controls = new OrbitControls(camera, renderer.domElement);
    controls.minDistance = 20;
    controls.maxDistance = 800;
    controls.maxPolarAngle = 2 * Math.PI;

    // ambient light, to have minimum lighting
    scene.add(new AmbientLight(0xffffff, 0.1));

    // directional light to simulate the sun
    const directionalLight = new DirectionalLight(0xffffff, 1.1);
    directionalLight.position.set(0, 0, 1).normalize();
    scene.add(directionalLight);

    const helper = new DirectionalLightHelper(directionalLight, 50);
    scene.add(helper);

    scene.add(new AxesHelper(30));

    // textures
    const earthGroup = createGroup('earthGroup');
    scene.add(earthGroup);

    // points
    const earthRadius = 15;

    let textureLoader = new TextureLoader();
    const quality = 2;
    let sphereGeometry = new SphereGeometry(
        earthRadius,
        32 * quality,
        16 * quality
    );
    // adapt to a ellipsoid
    const squishFactor = 6356.7523 / 6378.127; // compute to 0.996

    sphereGeometry.applyMatrix4(
        new Matrix4().makeScale(1.0, squishFactor, 1.0)
    );

    let material = new MeshPhongMaterial();

    material.map = textureLoader.load("images/earth_base_map_10k.jpg");
    material.bumpMap = textureLoader.load("images/earth_bump_map.jpg");
    material.bumpScale = 0.05;

    material.specularMap = textureLoader.load("images/earth_specular_map.jpg");
    material.specular = new Color(
        new Color("rgb(255,255,255)").multiplyScalar(0.1)
    );

    let earthMesh = new Mesh(sphereGeometry, material);

    earthGroup.add(earthMesh);

    // let lowerAtmosphere = getAtmosphereMaterial();
    // lowerAtmosphere.uniforms.glowColor.value.set(0x00b3ff);
    // lowerAtmosphere.uniforms.coeficient.value = 0.8;
    // lowerAtmosphere.uniforms.power.value = 2.0;
    // let lowerAtmosphereMesh = new Mesh(sphereGeometry, lowerAtmosphere);
    // lowerAtmosphereMesh.scale.multiplyScalar(1.01);
    // earthGroup.add(lowerAtmosphereMesh);

    // let upperAtmosphere = getAtmosphereMaterial();
    // upperAtmosphere.side = BackSide;
    // upperAtmosphere.uniforms.glowColor.value.set(0x00b3ff);
    // upperAtmosphere.uniforms.coeficient.value = 0.5;
    // upperAtmosphere.uniforms.power.value = 8;
    // let upperAtmosphereMesh = new Mesh(sphereGeometry, upperAtmosphere);
    // upperAtmosphereMesh.scale.multiplyScalar(1.1);
    // earthGroup.add(upperAtmosphereMesh);

    // add a line to show the inclination
    let inclinationLineMaterial = new MeshBasicMaterial({
        color: 0xffff00, opacity: 0.5, transparent: true
    })
    
    let cylinder = new Mesh(
        new CylinderGeometry(0.1, 0.1, 50, 8),
        inclinationLineMaterial
    );
    earthGroup.add(cylinder);

    let earthContainerGroup = createGroup('earthContainerGroup');
    earthContainerGroup.add(earthGroup);
    scene.add(earthContainerGroup);

    // earth inclination
    const inclination = degToRad(-23.4369);
    earthContainerGroup.rotateZ(inclination);

    // add to earth an athmosphere
    // let atmosphereGeometry = new THREE.SphereGeometry(earthRadius+0.2*earthRadius, 32, 16)
    // let atmosphereMaterial = new THREE.MeshPhongMaterial()
    // atmosphereMaterial.map = textureLoader.load('images/earth_clouds_map.png')
    // atmosphereMaterial.transparent = true
    // atmosphereMaterial.opacity = 0.5
    // let atmosphereMesh = new THREE.Mesh(atmosphereGeometry, atmosphereMaterial)
    // group.add(atmosphereMesh)

    window.addEventListener('resize', () => {
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();

        renderer.setSize(window.innerWidth, window.innerHeight);
    });

    return { scene, camera, controls, renderer };
};

export default initView
