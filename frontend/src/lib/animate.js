import { get } from "svelte/store";
import { clamp } from "three/src/math/MathUtils";
import { isPlaying, rate, epoch, metrics } from "./store";

let lastTimestamp;

// in seconds
const ROTATION_PERIOD = 86164.098;

function animate(context) {
    const { renderer, scene, camera, frameCycleCount } = context;
    return (timestamp) => {
        let metricsStartTimestamp;
        if (frameCycleCount === 0) {
            metricsStartTimestamp = Date.now()
        }
        if (!lastTimestamp) lastTimestamp = timestamp;

        const elapsed = !timestamp ? 0 : (timestamp - lastTimestamp);
        lastTimestamp = timestamp;

        if (get(isPlaying)) {
            let lastEpoch = get(epoch);
            // how many milliseconds have elapsed since the last frame in our simulation
            let dt = elapsed * get(rate);
            let newEpoch = lastEpoch + dt;
            epoch.set(newEpoch);

            // the earth do a rotation every sideral day
            // the earth do a rotation every 86 164.098 seconds
            /*
            0 0
            ROTATION_PERIOD 2*pi
            dt ?
            ? = dt * 2*pi / ROTATION_PERIOD
            */
            scene.getObjectByName('earthGroup').rotation.y +=
                (dt/1000)*2*Math.PI / ROTATION_PERIOD;
        }

        renderer.render(scene, camera);

        if (frameCycleCount === 0) {
            // let graphicsElasped = (Math.abs(metricsStartTimestamp-Date.now()))
            // const fps = Math.round(1000 / (graphicsElasped));
            // console.log(fps, graphicsElasped)
            // metrics.setFPS(clamp(fps, 0, 500));
        }

        context.frameCycleCount = (context.frameCycleCount + 1) % 10;
        requestAnimationFrame(animate(context));
    }
}

export default animate;
