const SOCKET_CLOSED = 0;
const SOCKET_OPENED = 1;

class SocketConnexion {
    constructor () {
    }

    connect () {
        return new Promise((resolve, reject) => {
            this.socket = new WebSocket('ws://localhost:8082');
            this.socket.addEventListener('open', (event) => {
                this.status = SOCKET_OPENED;
                resolve()
            });
            this.socket.addEventListener('close', (e) => {
                console.warn('WS closed', e)
                this.status = SOCKET_CLOSED;
            })
            this.socket.addEventListener('error', (e) => {
                console.error('WS Error', e);
            })
        })
    }

    isOpened () {
        return this.status === SOCKET_OPENED;
    }

    sendMessage(data) {
        return new Promise((resolve, reject) => {
            if (!this.isOpened()) {
                // stack the message
                this.socket.addEventListener('open', () => {
                    this.sendMessage(data).then(resolve).catch(reject)
                });
                return
            }
            
            this.socket.send(JSON.stringify(data));
            this.socket.addEventListener('message', (event) => {
                const data = JSON.parse(event.data)
                resolve(data)
            });
        })
    }

    getEarthParams () {
        return this.sendMessage({
            command: 'earth_params',
            args: {
                epoch: 1650105726
            }
        })
    }
}

export { SocketConnexion }