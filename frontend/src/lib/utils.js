import { Group } from "three"

const degToRad = (deg) => (deg * Math.PI) / 180

const clamp = (value, min, max) => {
    if (value == Infinity) {
        return max
    }
    if (value == NaN) {
        return min
    }
    if (value < min) {
        return min
    }
    if (value > max) {
        return max
    }
    return value
}

const createGroup = (name) => {
    const group = new Group()
    group.name = name
    return group
}

export { degToRad, createGroup, clamp }
