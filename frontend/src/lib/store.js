import { writable } from 'svelte/store';

function createIsPlayingStore() {
    const { subscribe, set, update } = writable(true);

    return {
        subscribe,
        toggle: () => update(status => !status),
        pause: () => set(false),
        play: () => set(true)
    };
}

export const isPlaying = createIsPlayingStore();

function createRateStore() {
    const { subscribe, set } = writable(1);

    return {
        subscribe,
        set: (newRate) => set(newRate),
    };
}

export const rate = createRateStore();

function createEpochStore() {
    // in milliseconds
    const { subscribe, set } = writable(Date.now());

    return {
        subscribe,
        set: (newEpoch) => set(newEpoch),
    };
}

export const epoch = createEpochStore();

function createMainSceneStore () {
    const { set, subscribe } = writable({});

    return { set, subscribe };
}

export const mainScene = createMainSceneStore();


function createMainCameraStore () {
    const { set, subscribe } = writable({});

    return { set, subscribe };
}

export const mainCamera = createMainCameraStore();

function createMetricsStore() {
    const { subscribe, set, update } = writable({ fps: 0 });

    return {
        subscribe,
        set,
        setFPS: (newFPS) => update(metrics => ({ ...metrics, fps: newFPS })),
        update
    };
}

export const metrics = createMetricsStore();

