import { Color, ShaderMaterial } from "three"

function getAtmosphereMaterial() {
    const vertexShader = [
        "varying vec3	vVertexWorldPosition;",
        "varying vec3	vVertexNormal;",

        "void main(){",
        "	vVertexNormal	= normalize(normalMatrix * normal);",

        "	vVertexWorldPosition	= (modelMatrix * vec4(position, 1.0)).xyz;",

        "	// set gl_Position",
        "	gl_Position	= projectionMatrix * modelViewMatrix * vec4(position, 1.0);",
        "}",
    ].join("\n")
    const fragmentShader = [
        "uniform vec3	glowColor;",
        "uniform float	coeficient;",
        "uniform float	power;",

        "varying vec3	vVertexNormal;",
        "varying vec3	vVertexWorldPosition;",

        "void main(){",
        "	vec3 worldCameraToVertex= vVertexWorldPosition - cameraPosition;",
        "	vec3 viewCameraToVertex	= (viewMatrix * vec4(worldCameraToVertex, 0.0)).xyz;",
        "	viewCameraToVertex	= normalize(viewCameraToVertex);",
        "	float intensity		= pow(coeficient + dot(vVertexNormal, viewCameraToVertex), power);",
        "	gl_FragColor		= vec4(glowColor, intensity);",
        "}",
    ].join("\n")

    return new ShaderMaterial({
        uniforms: {
            coeficient: {
                value: 1.0,
            },
            power: {
                value: 2,
            },
            glowColor: {
                value: new Color("pink"),
            },
        },
        vertexShader,
        fragmentShader,
        //blending	: THREE.AdditiveBlending,
        transparent: true,
        depthWrite: false,
    })
}

export default getAtmosphereMaterial
