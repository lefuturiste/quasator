const getOriginStateVector = (date) => {
    // get the position in space of the point (0,0) equator/prime meridian at the given date
    /*
    in the context of the point (0,0) located on the earth surface
    At local time noon, this point is more or less facing directly the sun
    or at least the prime meridian is aligned with the vector Sun-Earth


    Search for the most recent point in time where the meridian was aligned with the vector Sun-Earth
    Then compute the difference in time between that last time and the given date
    Then compute the angular difference between that time and the given date
    Finally return what the earth rotation would be at that time
    */
    
}
