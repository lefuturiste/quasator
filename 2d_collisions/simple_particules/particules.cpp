#define OLC_PGE_APPLICATION
#include "olcPixelGameEngine.h"
#include "cmath"

#define SCREEN_WIDTH 512
#define SCREEN_HEIGHT 512
#define POINTS_COUNT 50

class Example : public olc::PixelGameEngine
{
private:
	float particulesPx[POINTS_COUNT];
	float particulesPy[POINTS_COUNT];
	float particulesVx[POINTS_COUNT];
	float particulesVy[POINTS_COUNT];

public:
	Example()
	{
		sAppName = "Particules system";
	}

public:
	float RandFloat()
	{
		float f = (float(rand())/float(RAND_MAX));
		if (rand() > RAND_MAX/2) {
			f = -f;
		} 
		return f;
	}

	bool OnUserCreate() override
	{
		float speed = 60;
		for (int i = 0; i < POINTS_COUNT; i++) {
			particulesPx[i] = (float) (rand() % ScreenWidth());
			particulesPy[i] = (float) (rand() % ScreenHeight());
			particulesVx[i] = speed*RandFloat();
			particulesVy[i] = speed*RandFloat();
		}

		return true;
	}

	float Dist(int x1, int y1, int x2, int y2) {
		return sqrt(pow(x1-x2, 2) + pow(y1-y2, 2));
	}

	bool OnUserUpdate(float fElapsedTime) override
	{
		Clear(olc::BLACK);
		for (int i = 0; i < POINTS_COUNT; i++) {
			//printf("px: %f py: %f vx: %f vy: %f\n", particulesPx[i], particulesPy[i], particulesVx[i], particulesVy[i]);
			particulesPx[i] += particulesVx[i] * fElapsedTime;
			particulesPy[i] += particulesVy[i] * fElapsedTime;
			if (particulesPx[i] < 0 || particulesPx[i] > ScreenWidth()) {
				particulesVx[i] = -particulesVx[i];
			}
			if (particulesPy[i] < 0 || particulesPy[i] > ScreenHeight()) {
				particulesVy[i] = -particulesVy[i];
			}
			FillCircle(
				{ int(particulesPx[i]), int(particulesPy[i]) },
				2, olc::CYAN
			);
		}

/*
		for (int i = 0; i < POINTS_COUNT; i++) {
			for (int j = 0; j < POINTS_COUNT; j++) {
				if (Dist(particulesPx[i], particulesPy[i], particulesPx[j], particulesPy[j]) <= 2) {
					particulesVx[i] = -particulesVx[i];
					particulesVx[i] = -particulesVx[i];
					particulesVx[j] = -particulesVx[j]
				}
			}
		}
*/
		return true;
	}
};


int main()
{
	Example demo;
	if (demo.Construct(SCREEN_WIDTH, SCREEN_HEIGHT, 4, 4))
		demo.Start();

	return 0;
}

