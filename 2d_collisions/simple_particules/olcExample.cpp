#define OLC_PGE_APPLICATION
#include "olcPixelGameEngine.h"
#include "cmath"

#define POINTS_COUNT 10

class Example : public olc::PixelGameEngine
{
private:
	int setPointsX[POINTS_COUNT];
	int setPointsY[POINTS_COUNT];

	int setPointsR[POINTS_COUNT];
	int setPointsG[POINTS_COUNT];
	int setPointsB[POINTS_COUNT];

	int grabbedPoint = -1;

public:
	Example()
	{
		sAppName = "Voronoi diagram";
	}

public:
	bool OnUserCreate() override
	{
		for (int i = 0; i < POINTS_COUNT; i++) {
			int x = rand() % ScreenWidth();
			int y = rand() % ScreenHeight();
			setPointsX[i] = x;
			setPointsY[i] = y;
			setPointsR[i] = rand() % 255; 
			setPointsG[i] = rand() % 255;
			setPointsB[i] = rand() % 255;
			printf("x: %d y: %d\n", x, y);
		}
		UpdateScreen();

		return true;
	}

	float Dist(int x1, int y1, int x2, int y2) {
		return sqrt(pow(x1-x2, 2) + pow(y1-y2, 2));
	}

	void DrawPixel(int x, int y)
	{
		float minDist = -1;
		int minPoint = 0;
		// improve the way it's traced
		for (int i = 0; i < POINTS_COUNT; i++) {
			if (setPointsX[i] == x && setPointsY[i] == y) {
				Draw(x, y, olc::Pixel(255, 255, 255));
				return;
			}
			float dist = Dist(setPointsX[i], setPointsY[i], x, y);
			if (minDist == -1 || dist < minDist) {
				minDist = dist;
				minPoint = i;
			}
		}
		if (minDist < 5) {
			Draw(x, y, olc::Pixel(0, 0, 0));
			return;
		}
		Draw(x, y, olc::Pixel(setPointsR[minPoint], setPointsG[minPoint], setPointsB[minPoint]));
	}

	void UpdateScreen()
	{
		for (int x = 0; x < ScreenWidth(); x++) {
			for (int y = 0; y < ScreenHeight(); y++) {
				DrawPixel(x, y);
			}
		}
    	int mouseX = GetMouseX();
    	int mouseY = GetMouseY();

	    if (GetMouse(0).bHeld) {			
	    	FillCircle({ mouseX, mouseY }, 5, olc::CYAN);

			// get the closest point
			float minDist = -1;
			int minPoint = 0;
			for (int i = 0; i < POINTS_COUNT; i++) {
				float dist = Dist(setPointsX[i], setPointsY[i], mouseX, mouseY);
				if (minDist == -1 || dist < minDist) {
					minDist = dist;
					minPoint = i;
				}
			}
			// grab
			if (minDist < 5) {
				grabbedPoint = minPoint;
			}
	    }
	    if (!GetMouse(0).bHeld) {
	    	grabbedPoint = -1;
	    }
	    if (grabbedPoint >= 0) {
			setPointsX[grabbedPoint] = mouseX;
			setPointsY[grabbedPoint] = mouseY;
	    }
	}

	bool OnUserUpdate(float fElapsedTime) override
	{
		UpdateScreen();
		return true;
	}
};


int main()
{
	Example demo;
	if (demo.Construct(512, 512, 4, 4))
		demo.Start();

	return 0;
}

