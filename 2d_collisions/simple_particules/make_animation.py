import numpy as np
import cv2
import os
import random

video_name = 'video.avi'

height, width = 100, 100

def random_color():
    return [
        random.randint(0, 255),
        random.randint(0, 255), 
        random.randint(0, 255)
    ]

video = cv2.VideoWriter(video_name, 0, 1, (width,height))

for i in range(0, 100):
    arr = np.array([
        [random_color() for j in range(0, width)]
        for i in range(0, height)
    ])
    arr = cv2.cvtColor(arr.astype(np.uint8), cv2.COLOR_BGR2RGB)
    video.write(arr)

cv2.destroyAllWindows()
video.release()