# the basic idea behind sweep and prune is to first sort the items by position 
"""
- Have a stack of pair to check
- Sort the item by x then by y
- Sweep the items on the X axis and on the Y axis simultaneously
- If a pair come close in X and Y then push the pair on the suspected pair stack
- Do the detection collision on the stack
- Using sorting algorithm like insertion sort allow us to do minimum amount of operations to get a new sorted list of items by X and Y position after a new tick (because probably nothing wont change)
- So for each tick we have
    - n operations to sort by X
    - n operations to sort by Y
    - 2*n operations to check pairs
    - then process the suspected pairs
    - that is O(n)
- This is an improvement over O(n^2)
"""
