import sys
import numpy as np
import random
import json
import time
from decimal import Decimal


# def bytes_from_float(ft):
#     compos = str(ft).split('.')
#     e = int(compos[0])
#     q = int(compos[1])/(10**len(compos[1]))
#     return (e, q)

# f = open('./scenario_1', 'wb')
# f.write(bytes(positions_history))

# x = 23.42323439834
# print(bytes_from_float(x))
# exit()

NB_PARTICULES = 200
FPS = 30
ANIMATION_DURATION = 60 # one minute
FLOAT_MAN_SIZE=6 # in bytes
FLOAT_EXP_SIZE=2 # in bytes
COORDINATE_SIZE=4 # standard int allocation in bytes
COORDINATE_MAX=2**(COORDINATE_SIZE*8) - 1
#G = 6.67430*10**(-11)

positions = np.zeros((NB_PARTICULES, 2))
speeds = np.zeros((NB_PARTICULES, 2))

def random_vector():
    return np.array([random.uniform(0, 1), random.uniform(0, 1)])

def random_speed():
    return np.array([random.uniform(0, 1), random.uniform(0, 1)])


# initialization of the speeds and particules

positions_history = []

masses = [0]*NB_PARTICULES
for p in range(NB_PARTICULES):
    positions[p] = random_vector()
    speeds[p] = 0.01*random_speed()
    masses[p] = random.gauss(10**20, 30)

# 0, 0 at the top
# 0 → 1
# ↓
# 1

"Pre compute the file size before hand"
fsize = FPS*ANIMATION_DURATION*NB_PARTICULES*2*(COORDINATE_SIZE)
print(f'Will be generating a {fsize} bytes scenario file')

collisions = []

for f, frame in enumerate(range(FPS*ANIMATION_DURATION)):
    for p in range(NB_PARTICULES):
        old_pos = positions[p].copy()
        positions[p] = positions[p] + speeds[p]
        # collision detection with the walls
        coll = False
        if positions[p][0] < 0 or positions[p][0] > 1 or positions[p][1] < 0 or positions[p][1] > 1:
            speeds[p] = -speeds[p]
            coll = True

        if coll:
            positions[p] = old_pos + speeds[p]
        #introduce random acceleration
        # accel = 0
        # for k in range(NB_PARTICULES):
        #     if k == p: continue
        #     accel += G*masses[k]*masses[p]/((np.linalg.norm(positions[k]-positions[p]))**2)
        # speeds[p] += accel/masses[p]
    
    # Now detect collisions with the new positions and log them
    # generate the quad tree

    positions_history.append(positions.copy())

def fexp(number):
    (sign, digits, exponent) = Decimal(number).as_tuple()
    return len(digits) + exponent - 1

def fman(number):
    return Decimal(number).scaleb(-fexp(number))

def clamp(subject, min, max):
    if subject < min:
        return min
    if subject > max:
        return max
    return subject

l = 0
file_content = bytearray()
for f, frame_positions in enumerate(positions_history):
    if f < 10:
        print(
            frame_positions[0],
            frame_positions[1]
        )
    for p in range(NB_PARTICULES):
        # for each particule we encode two floats (position.x and position.y)
        # each of the float are encoded with 4 bytes for the integer part and 6 bytes for the float part
        for i in range(0, 2):
            ft = frame_positions[p][i]
            ft = int(clamp(ft, 0, 1)*COORDINATE_MAX)

            file_content.extend(
                ft.to_bytes(
                    COORDINATE_SIZE,
                    byteorder=sys.byteorder,
                    signed=False
                ))

#print(file_content)
f = open('./scenario_1', 'wb')
f.write(file_content)
f.close()

# write the parameters
f = open('./parameters', 'w')
f.write(f"particules={NB_PARTICULES}\n")
f.write(f"fps={FPS}\n")
f.write(f"duration={ANIMATION_DURATION}\n")
f.write(f"dimensions={2}\n")
f.write(f"coordinate_size={COORDINATE_SIZE}\n")
f.write(f"created_at={int(time.time())}\n")
f.close()

# f = open('./debug.json', 'w')
# f.write(json.dumps(debug))
f.close()
