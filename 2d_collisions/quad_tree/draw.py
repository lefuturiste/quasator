from ..utils import clamp
from .Node import Node

colors = [
    # "#1abc9c",
    # "#2ecc71",
    "#3498db",
    "#27ae60",
    # "#9b59b6",
    # "#34495e",
    # "#16a085",
    # "#27ae60",
    # "#2980b9",
    # "#8e44ad",
    "#2c3e50",
    "#f1c40f",
    "#e67e22",
    "#e74c3c",
]
def draw_tree(plt, node: Node, max_depth = None):
    if len(node.children_nodes) == 0:
        return
    bbox, depth, med = node.bbox, node.depth, node.get_med()

    if max_depth != None and depth > max_depth:
        return
    
    lw = clamp(3-node.depth, 1, 3)
    plt.plot(
        [bbox[0].x, bbox[1].x], [med.y, med.y],
        linewidth=lw,
        color=colors[depth%len(colors)]
    )
    plt.plot(
        [med.x, med.x],
        [bbox[0].y, bbox[1].y],
        linewidth=lw,
        color=colors[depth%len(colors)]
    )

    for cn in node.children_nodes:
        draw_tree(plt, cn, max_depth)

