
import json
import timeit
from .construct import construct_tree
from ..utils import Point
from .range_search import range_search
import multiprocessing
import time

results = []

RADIUS=0.1

def run_quad_tree(nb_of_points, bucket_size):
    points = []
    for i in range(nb_of_points):
        x, y = round(random.uniform(0, 1), 3), round(random.uniform(0, 1), 3)
        points.append(Point(x, y, i))
    def timer_wrapper():
        tree = construct_tree(points, bucket_size=bucket_size)
        incidents = []
        for i in range(nb_of_points):
            found = range_search(tree, points[i], RADIUS)
            for f in found:
                incidents.append((i, f))

    return timeit.timeit(timer_wrapper, number=20)

if __name__ == '__main__':
    import random
    
    results = []
    NB_OF_POINTS = 5_000

    def run(bs):
        time_res = run_quad_tree(NB_OF_POINTS, bs)
        print(f'> bucket size: {bs} got: {time_res}')
        return {
            'bucket_size': bs,
            'nb_of_points': NB_OF_POINTS,
            'duration': time_res
        }

    pool = multiprocessing.Pool(processes=6)
    outputs = pool.map(run, list(range(3, 100)))
    print(outputs)

    c = json.dumps({
        'created_at': time.time(),
        'data': outputs
    }, indent=2)

    f = open('./timing_result_quad_tree_optimum_1.json', 'w')
    f.write(c)
    f.close()

