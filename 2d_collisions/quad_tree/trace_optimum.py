import tikzplotlib
import matplotlib.pyplot as plt
import json

def export_figs(label):
    plt.savefig(f'./graphs/{label}.png')
    tikzplotlib.save(f'./graphs/{label}_src.tex')

# f = open('./2d_collisions/quad_tree/quad_tree_optimum_search_res_1.txt', 'r')
# raw_content = f.read()

# entries = [[float(compo) for compo in l.split(' ') if compo != ''] for l in raw_content.split('\n') if l != '']

f = open('./2d_collisions/quad_tree/timing_result_quad_tree_optimum_1.json')
rawJson = json.loads(f.read())
entries = [[e['bucket_size'], e['duration']] for e in rawJson['data'] ]

ids = list(sorted([int(e[0]) for e in entries]))

X = []
Y1 = []
for _id in ids:
    # find the entry that match the id
    entry = [e for e in entries if e[0] == _id][0]
    X.append(int(_id))
    Y1.append(entry[1])

fig, axs = plt.subplots(1, 1)
axs.plot(X, Y1, 'b')

axs.plot(X, Y1, 'bd', markersize=5)
axs.grid(True)

#plt.legend(['Algo naïf', 'Algo kd-tree', 'Algo quad-tree'])

#axs.set_title("Recherche d'un optimum pour l'algorithme de recherche quad-tree")
axs.set_xlabel("Nombre de points dans une feuille")
axs.set_ylabel("Durée d'execution CPU (en sec)")

export_figs('quad_tree_optimum_new')

#axs.set_xscale('log')
#axs.set_yscale('log')

plt.show()
