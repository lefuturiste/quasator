from typing import NamedTuple

class Point(NamedTuple):
    x: float
    y: float
    id: int = None

    def set_id(self, id):
        self.id = id
