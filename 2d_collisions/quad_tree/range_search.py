from ..utils import Point
from .Node import Node
from .construct import in_partition

from ..utils import distance, project

def simple_range_search(subjects: list[Point], poi: Point, radius: float):
    res = []
    for s in subjects:
        if s.id == poi.id: continue
        if distance(s, poi) < radius:
            res.append(s.id)
    return res

def range_search_in_child(node: Node, poi: Point, radius: float):
    if distance(project(node.bbox, poi), poi) > radius:
        return []
    if len(node.children_points) > 0:
        return simple_range_search(node.children_points, poi, radius)
    if len(node.children_nodes) == 0:
        return []
    res = []
    for i in range(4):
        child = node.children_nodes[i]
        if distance(project(child.bbox, poi), poi) < radius:
            res.extend(range_search_in_child(child, poi, radius))
    return res

def range_search(node: Node, poi: Point, radius: float):
    if (
        len(node.children_points) > 0 and
        len(list(filter(lambda p: p.id == poi.id, node.children_points))) > 0
    ):
        # we found the point
        # now, do a simple research with those in the same box
        # then, do the search in reverse,
        # at each parent, project the bbox and analyse etc.
        return simple_range_search(node.children_points, poi, radius)
    if len(node.children_points) > 0:
        raise ValueError('range_search: Dead end')
    if len(node.children_nodes) == 0:
        raise ValueError('range_search: Malformed tree')
    
    med = node.get_med()
    
    selected_children = in_partition(med, poi)
    res = range_search(node.children_nodes[selected_children], poi, radius)
    # we will check each of the other 3 children that we have

    for i in range(4):
        if i == selected_children: continue
        res.extend(range_search_in_child(node.children_nodes[i], poi, radius))
    return res

