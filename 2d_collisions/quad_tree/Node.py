from typing import NamedTuple
from ..utils import Point

class Node(NamedTuple):

    # A list with 4 children of type Node
    children_nodes: list

    # A list with MAX_BUCKET_SIZE of type Point
    children_points: list[Point]

    depth: int
    bbox: tuple[Point, Point]

    def get_med(self):
        return Point(
            (self.bbox[1].x+self.bbox[0].x)/2,
            (self.bbox[1].y+self.bbox[0].y)/2
        )
    
