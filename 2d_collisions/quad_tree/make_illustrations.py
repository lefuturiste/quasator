import tikzplotlib
from .range_search import range_search
from .draw import draw_tree
from ..utils import distance, project
from .construct import construct_tree
from itertools import chain
from .Point import Point

def init_plot():
    plt.clf()
    plt.axis('scaled')
    plt.xlim(0, 1)
    plt.ylim(0, 1)

def draw_points(points):
    # add labels
    for i, x, y in points:
        plt.text(x,y, f"{i}")

    plt.plot([x for _,x,_ in points], [y for _,_,y in points], '.k', markersize=4)

def export_figs(label, mode = 'construct'):
    plt.savefig(f'./graphs/quad_tree_{mode}/{label}.png')
    tikzplotlib.save(f'./graphs/quad_tree_{mode}/{label}_src.tex', extra_axis_parameters=['axis equal image'])

def get_bbox(pid):
    return list(filter(lambda x: x.id == pid, all_points))[0].bbox

def draw_bbox(plot, pid):
    bbox = get_bbox(pid)
    
    artist = plot.Polygon([
        bbox[0],
        (bbox[1][0], bbox[0][1]),
        bbox[1],
        (bbox[0][0], bbox[1][1]),
    ], hatch="\\\\", facecolor='#27ae600f')
    plot.gca().add_artist(artist)

if __name__ == '__main__':
    import matplotlib.pyplot as plt
    import random

    
    points = []
    projected = []
    
    id_c = 0
    # for i in range(15):
    #     x, y = round(random.uniform(0, 1), 3), round(random.uniform(0, 1), 3)
    #     if len(list(filter(lambda p: p[1] == x or p[2] == y, points))) > 0:
    #         continue
    #     points.append((id_c, x, y))
    #     id_c += 1

    points = [
        (1, 0.5, 0.5),
        (2, 0.08, 0.23),
        (3, 0.23, 0.3),
        (4, 0.65, 0.38),
        (5, 0.75, 0.18),
        (6, 0.74, 0.52),
        (7, 0.1156, 0.43),
        (8, 0.98, 0.14),
        (9, 0.26, 0.87),
        (10, 0.36, 0.22),
        (11, 0.15, 0.16),
        (12, 0.85, 0.15),
        (13, 0.6, 0.91),
        (14, 0.9, 0.541),
        (15, 0.75, 0.32),
        (16, 0.88, 0.16),
        (17, 0.66, 0.14),
        (18, 0.135, 0.30),
        (19, 0.45, 0.36),
        (20, 0.1689, 0.76),
        (21, 0.43, 0.25),
        (22, 0.38, 0.81),
        (23, 0.4158, 0.95),
        (24, 0.55, 0.60),
        (25, 0.20, 0.62),
        (26, 0.21, 0.9),
    ]


    points_c = [ Point(p[1], p[2], p[0]) for p in points ]

    res = construct_tree(points_c, bucket_size=2)

    # generate each step of the construction
    for drawing_depth in [0, 1, 2, 3, 4]:
        init_plot()
        plt.plot([x for _,x,_ in points], [y for _,_,y in points], '.k', markersize=4)
        draw_tree(plt, res, drawing_depth)

        export_figs(drawing_depth, 'construct')
        print(f"wrote {drawing_depth}")

    # def explore_all(node):
    #     return [
    #         node.without_children(),
    #         *list(chain(
    #             *[explore_all(c) for c in node.children]
    #         ))
    #     ]

    # all_points = explore_all(res)

    # init_plot()
    # draw_tree(plt, res)
    # draw_points(points)
    # # slide 1: general situation
    # export_figs('situation')

    # # slide 2: situation but with the point and the disk radius of search
    # R = 0.2
    # # fill a disk
    # pId = 3
    # c1 = plt.Circle((points[pId-1][1], points[pId-1][2]), R, color="#ecf0f1")
    # plt.gca().add_artist(c1)
    # c2 = plt.Circle((points[pId-1][1], points[pId-1][2]), R, color="#e74c3c", fill=False)
    # plt.gca().add_artist(c2)

    
    # plt.plot(
    #     [x for i,x,_ in points if i != pId],
    #     [y for i,_,y in points if i != pId],
    #     '.k', markersize=4
    # )
    # plt.plot([points[pId-1][1]], [points[pId-1][2]], '.r', markersize=5)

    # export_figs('situation_focused')

    # # slide 3, explore children, compute radius with each children

    # init_plot()
    # draw_tree(plt, res)
    # plt.plot(
    #     [x for i,x,_ in points if i != pId],
    #     [y for i,_,y in points if i != pId],
    #     '.k', markersize=4
    # )
    # plt.plot([points[pId-1][1]], [points[pId-1][2]], '.r', markersize=5)

    # plt.plot([points[pId-1][1], points[2-1][1]], [points[pId-1][2], points[2-1][2]], 'b')
    # plt.plot([points[pId-1][1], points[21-1][1]], [points[pId-1][2], points[21-1][2]], 'g')

    # export_figs('explore_3')
    

    # # slide 4, project on to grand children bounding box
    # init_plot()
    # draw_tree(plt, res)
    # plt.plot(
    #     [x for i,x,_ in points if i != pId],
    #     [y for i,_,y in points if i != pId],
    #     '.k', markersize=4
    # )
    
    # draw_bbox(plt, 18)

    # plt.plot([points[pId-1][1]], [points[pId-1][2]], '.r', markersize=5)
    # c2 = plt.Circle((points[pId-1][1], points[pId-1][2]), R, color="#e74c3c", fill=False)

    # plt.gca().add_artist(c2)
    # export_figs('explore_4')

    # # slide 5, explore parent and explore other child parent
    # init_plot()
    # draw_tree(plt, res)

    # plt.plot(
    #     [x for i,x,_ in points if i != pId],
    #     [y for i,_,y in points if i != pId],
    #     '.k', markersize=4
    # )
    # plt.plot([points[pId-1][1]], [points[pId-1][2]], '.r', markersize=5)
    # c2 = plt.Circle((points[pId-1][1], points[pId-1][2]), R, color="#e74c3c", fill=False)
    # plt.gca().add_artist(c2)

    # draw_bbox(plt, 26)
    # # draw the projection
    # bbox1 = get_bbox(26)
    # projected = project(bbox1, [points[pId-1][1], points[pId-1][2]])
    # plt.plot([points[pId-1][1], projected[0]], [points[pId-1][2], projected[1]], linewidth=3, color='r')


    # export_figs('explore_5')


    # generate the illustration of the search algo
    