from .Node import Node
from ..utils import Point

DEFAULT_BUCKET_SIZE = 16

def in_partition(med: Point, p: Point):
    x = -1
    if p.x > med.x:
        x = 1
    y = -1
    if p.y > med.y:
        y = 1
    # bottom left
    if x == -1 and y == 1:
        return 0
    if x == 1 and y == 1:
        return 1
    if x == -1 and y == -1:
        return 2
    if x == 1 and y == -1:
        return 3
    raise ValueError('in_partition: cannot find partition')

def construct_tree(points: list[Point], depth = 0, bbox = (Point(0, 0), Point(1, 1)), bucket_size = DEFAULT_BUCKET_SIZE):
    """
    Construct the quad tree
    """
    node = Node([], [], depth, bbox)
    if len(points) < bucket_size:
        return Node([], points, depth, bbox)

    partition = [[], [], [], []]
    med = node.get_med()
    for p in points:
        partition[in_partition(med, p)].append(p)
    
    children = []
    for i, part in enumerate(partition):
        if i == 0:
            new_bbox = (
                Point(bbox[0].x, med.y),
                Point(med.x, bbox[1].y)
            )
        if i == 1:
            new_bbox = (
                Point(med.x, med.y),
                Point(bbox[1].x, bbox[1].y)
            )
        if i == 2:
            new_bbox = (
                Point(bbox[0].x, bbox[0].y),
                Point(med.x, med.y)
            )
        if i == 3:
            new_bbox = (
                Point(med.x, bbox[0].y),
                Point(bbox[1].x, med.y)
            )
        children.append(construct_tree(part, depth+1, new_bbox, bucket_size))
    
    return Node(children, [], depth, bbox)
