
import json
from .draw import draw_tree
from ..utils import distance
from .construct import construct_tree
from .Point import Point
from .Node import Node
from .range_search import range_search

def naive_range(points: list[Point], poi: Point, radius):
    res = []
    for p in points:
        if p.id == poi.id: continue
        if distance(p, poi) < radius:
            res.append(p.id)
    return res

def debug_tree_aux(tree: Node):
    if len(tree.children_nodes) == 0:
        return
    print(' '*tree.depth*2 + str(tuple(tree.bbox[0])) + '; ' + str(tuple(tree.bbox[1])))
    for i in range(4):
        debug_tree_aux(tree.children_nodes[i])

def debug_tree(tree: Node):
    print('--- DEBUG start')
    debug_tree_aux(tree)
    print('--- DEBUG end')

if __name__ == '__main__':
    import matplotlib.pyplot as plt
    import random

    plt.axis('scaled')
    plt.xlim(0, 1)
    plt.ylim(0, 1)
    
    points = []
    
    id_c = 0
    for i in range(200):
        x, y = round(random.uniform(0, 1), 3), round(random.uniform(0, 1), 3)
        points.append(Point(x, y))
        id_c += 1

    #print(points)

    points_r = [
        (0, 0.5, 0.5),
        (1, 0.4802, 0.48),
        (2, 0.52011, 0.5203),
        (3, 0.53, 0.5204),
        (4, 0.51023, 0.4809876),
        (5, 0.4850, 0.4978511),
        (6, 0.49512, 0.51359),
        (7, 0.48987, 0.51205),
        (77, 0.510, 0.474),
        (78, 0.520, 0.492),
        (79, 0.492, 0.478),
        (8, 0.9, 0.96),
        (9, 0.09, 0.03),
        (10, 0.72, 0.72),
        (11, 0.22, 0.22),
    ]

    #points_r = [(0, 0.499, 0.811), (1, 0.337, 0.686), (2, 0.84, 0.936), (3, 0.704, 0.974), (4, 0.713, 0.924), (5, 0.749, 0.146), (6, 0.364, 0.004), (7, 0.602, 0.163), (8, 0.332, 0.822), (9, 0.85, 0.097), (10, 0.722, 0.178), (11, 0.319, 0.497), (12, 0.938, 0.628), (13, 0.937, 0.682), (14, 0.504, 0.195), (15, 0.912, 0.514), (16, 0.467, 0.994), (17, 0.272, 0.74), (18, 0.498, 0.708), (19, 0.268, 0.296), (20, 0.677, 0.526), (21, 0.914, 0.702), (22, 0.049, 0.039), (23, 0.521, 0.975), (24, 0.679, 0.413), (25, 0.422, 0.732), (26, 0.565, 0.585), (27, 0.279, 0.144), (28, 0.797, 0.123), (29, 0.762, 0.646), (30, 0.622, 0.655), (31, 0.158, 0.357), (32, 0.644, 0.328), (33, 0.735, 0.122), (34, 0.576, 0.593), (35, 0.46, 0.17), (36, 0.804, 0.617), (37, 0.51, 0.635), (38, 0.115, 0.847), (39, 0.913, 0.337), (40, 0.101, 0.282), (41, 0.707, 0.915), (42, 0.977, 0.96), (43, 0.64, 0.168), (44, 0.238, 0.543), (45, 0.399, 0.312), (46, 0.891, 0.729)]

    points = [ Point(p[1], p[2], p[0]) for p in points_r ]

    res = construct_tree(points)
    
    #debug_tree(res)
    #print(res)
    print('POI is', points[0])
    radius = 0.2
    range_res = range_search(res, points[0], radius)
    naive_range_res = naive_range(points, points[0], radius)


    print(f'{range_res=}')
    print(f'{naive_range_res=}')
    print(set(naive_range_res) == set(range_res))
    
    draw_tree(plt, res)

    plt.plot([p.x for p in points], [p.y for p in points], '.k', markersize=4)
    for p in points:
        plt.text(p.x, p.y, str(p.id))
    plt.show()

