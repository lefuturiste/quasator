import matplotlib.pyplot as plt
import numpy as np

X = np.linspace(0, 1, 50)
plt.plot(X, 0.25*np.exp(X)*np.cos(2*X))
plt.show()
