from math import sqrt

from typing import NamedTuple

class Point(NamedTuple):
    x: float
    y: float
    id: int = None

    def set_id(self, id):
        self.id = id

def distance(p1: tuple[float, float], p2: tuple[float, float]):
    """
    Returns the euclidian distance between two points
    """
    return sqrt((p1[0]-p2[0])**2+(p1[1]-p2[1])**2)

def clamp(subject: float, min: float, max: float):
    """
    Clamp a value between two boudaries in one dimension
    """
    if subject < min:
        return min
    if subject > max:
        return max
    return subject

def project(bbox: tuple[list[float], list[float]], point: list):
    """
    get the coordinate of the projected point on the bounding box
    IMPORTANT: for now only work in two dimension
    project on the two lines
    line along x = bbox[0][0]
    line along y = bbox[0][1]
    """
    print(clamp(point[0], bbox[0][0], bbox[1][0]), point[0])
    return (
        clamp(point[0], bbox[0][0], bbox[1][0]),
        clamp(point[1], bbox[0][1], bbox[1][1])
    )

def naive_range(points: list[Point], poi: Point, radius):
    res = []
    for p in points:
        if p.id == poi.id: continue
        if distance(p, poi) < radius:
            res.append(p.id)
    return res
