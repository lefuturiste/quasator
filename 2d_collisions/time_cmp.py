# sandbox to test for range search
import time
import json
import timeit

from .utils import distance

from .kd_tree.range_search import range_search as kd_range_search
from .kd_tree.construct import construct_tree as kd_construct

from .quad_tree.Point import Point
from .quad_tree.range_search import range_search as qt_range_search
from .quad_tree.construct import construct_tree as qt_construct

import random

def naive_range(points, poi, poi_id, radius):
    res = []
    for p in points:
        if p[0] == poi_id: continue
        if distance(p[1:], poi) < radius:
            res.append(p[0])
    return res

def compare_algo(N):
    """
    Goal of the task: find every couple of POI within *warn_radius*
    for N points scattered randomly in a plane
    """
    RADIUS = 0.1
    points = []

    for i in range(N):
        x, y = random.uniform(0, 1), random.uniform(0, 1)
        points.append((i, x, y))

    def naive_implementation():
        incidents = []
        for i in range(N):
            found = naive_range(points, points[i][1:], i, RADIUS)
            for f in found:
                incidents.append((i, f))

    def kd_tree_implementation():
        tree = kd_construct(points)
        incidents = []
        for i in range(N):
            found = kd_range_search(tree, points[i][1:], i, RADIUS)
            for f in found:
                incidents.append((i, f))

    def to_point(p):
        return Point(p[1], p[2], p[0])

    def quad_tree_implementation():
        tree = qt_construct([ to_point(p) for p in points ])
        incidents = []
        for i in range(N):
            found = qt_range_search(tree, to_point(points[i]), RADIUS)
            for f in found:
                incidents.append((i, f))
    
    print(f'> With {N} points...')
    print('  naive...')
    nb_of_time = 20
    t1 = timeit.timeit(naive_implementation, number=nb_of_time)
    print('  got:', t1)
    print('  kd...')
    t2 = timeit.timeit(kd_tree_implementation, number=nb_of_time)
    print('  got:', t2)
    print('  qt...')
    t3 = timeit.timeit(quad_tree_implementation, number=nb_of_time)
    print('  got:', t3)

    return {
        'nb_of_points': N,
        'nb_of_runs': nb_of_time,
        'naive': t1,
        'kd_tree': t2,
        'qt_tree': t3
    }

if __name__ == '__main__':
    out = []
    for i in range(0, 4):
        for j in range(1, (6 if i == 3 else 10)):
            n = j*(10**i)
            res = compare_algo(n)
            print(' ', res)
            out.append(res)
    c = json.dumps({
        'created_at': time.time(),
        'data': out
    }, indent=2)
    f = open('./timing_result_6.json', 'w')
    f.write(c)
    f.close()
 