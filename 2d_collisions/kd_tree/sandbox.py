from .closest import get_closest
from .draw import draw_tree
from ..utils import distance
from .construct import construct_tree

if __name__ == '__main__':
    import matplotlib.pyplot as plt
    import random

    plt.axis('scaled')
    plt.xlim(0, 1)
    plt.ylim(0, 1)
    
    points = []
    projected = []
    
    id_c = 0
    for i in range(50):
        x, y = round(random.uniform(0, 1), 3), round(random.uniform(0, 1), 3)
        if len(list(filter(lambda p: p[1] == x or p[2] == y, points))) > 0:
            continue
        points.append((id_c, x, y))
        id_c += 1

    print(points)

    # points = [
    #     (0, 0.5, 0.5),
    #     (1, 0.4802, 0.48),
    #     (2, 0.52011, 0.5203),
    #     (3, 0.53, 0.5204),
    #     (4, 0.51023, 0.4809876),
    #     (5, 0.9, 0.96),
    #     (6, 0.09, 0.03)
    # ]

    res = construct_tree(points)
    #print(res)
    
    draw_tree(plt, res)

    # print('searching...')
    poi_id = random.randint(0, len(points)-1)
    poi = points[poi_id]
    print('POI is', poi)
    # get the closest point using conventional methods
    dists = [ (p, distance(p[1:], poi[1:])) for p in points if p[0] != poi_id ]
    min_p = min(dists, key=lambda p: p[1])
    print('min_p', min_p)
    closest = get_closest(res, poi[1:], poi_id)
    print(closest)

    if not closest or min_p[0][0] != closest[0]:
        print('ERROR')
    else:
        print('SUCCESS')

    plt.plot([x for _,x,_ in points], [y for _,_,y in points], '.k', markersize=4)
    #plt.plot([x for x,_ in projected], [y for _,y in projected], '.r')
    plt.show()

