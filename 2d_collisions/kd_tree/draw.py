from ..utils import clamp

import matplotlib.colors as mcolors

#colors_keys = list(mcolors.CSS4_COLORS.keys())
colors_keys = [
    'blue',
    'orange',
    'green',
    'red',
    'purple',
    'brown',
    'pink',
    'gray',
    'olive',
    'cyan'
]
colors = [
    # "#1abc9c",
    # "#2ecc71",
    "#3498db",
    "#27ae60",
    # "#9b59b6",
    # "#34495e",
    # "#16a085",
    # "#27ae60",
    # "#2980b9",
    # "#8e44ad",
    "#2c3e50",
    "#f1c40f",
    "#e67e22",
    "#e74c3c",
]

def draw_tree(plt, node, max_depth = None):
    """
    draw a k-d tree with matplotlib
    for now, this func only work in dimension 2
    """
    med, axis, depth, bbox = node.point, node.axis, node.depth, node.bbox
    if max_depth != None and depth > max_depth:
        return

    if axis == 0: # vertical line
        plt.plot(
            [med[0], med[0]], [bbox[0][1], bbox[1][1]],
            linewidth=clamp(3-depth, 1, 3),
            color=colors[depth%len(colors)]
        )
    if axis == 1: # horizontal line
        plt.plot(
            [bbox[0][0], bbox[1][0]], [med[1], med[1]],
            linewidth=clamp(3-depth, 1, 3),
            color=colors[depth%len(colors)]
        )
    
    children = node.children
    if len(children) >= 1:
        draw_tree(plt, children[0], max_depth)
    if len(children) >= 2:
        draw_tree(plt, children[1], max_depth)
