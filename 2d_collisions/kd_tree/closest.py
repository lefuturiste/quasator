from ..utils import distance, project

def closest_in_child(node, poi, min_id, min_dist):
    """
    Internal function used by get_closest
    """
    d = distance(node.point, poi)
    if min_dist == -1 or d < min_dist:
        min_dist = d
        min_id = node.id
    res = (min_id, min_dist)
    if len(node.children) >= 1:
        if distance(project(node.children[0].bbox, poi), poi) < min_dist:
            res = closest_in_child(node.children[0], poi, min_id, min_dist)
    if len(node.children) >= 2:
        if distance(project(node.children[1].bbox, poi), poi) < min_dist:
            res = closest_in_child(node.children[1], poi, min_id, min_dist)
    return res

def get_closest(node, poi, poi_id):
    """
    get the closest point from a POI in a tree
    POI = point of interest to search
    """
    # STEP 1: go down the tree to find the given point
    # then try to project this point to many bounding boxes
    # print('cmp', node.id, poi_id, node.id == poi_id)
    if node.id == poi_id:
        # we found the point
        res = (-1, -1)
        # it may have children, so we call the search child on these
        if len(node.children) >= 1:
            res = closest_in_child(
                node.children[0],
                poi, -1, -1
            )
        if len(node.children) >= 2:
            res = closest_in_child(
                node.children[1],
                poi, res[0], res[1]
            )
        return res
    
    # dead end, should never happen
    # FIXME: change this to a ValueError
    if not node.has_children(): return False

    # search the fck point
    if poi[node.axis] < node.point[node.axis]:
        side = 0
    if poi[node.axis] > node.point[node.axis]:
        side = 1
    
    res = get_closest(node.children[side], poi, poi_id)
    # dead end, shoudl never happen
    # FIXME: change this to a ValueError
    if not res: return False
    
    d = distance(node.point, poi)
    min_id, min_dist = res
    if min_dist == -1 or d < min_dist:
        min_id = node.id
        min_dist = d

    if len(node.children) == 2:
        return closest_in_child(
            node.children[0 if side == 1 else 1],
            poi,
            min_id,
            min_dist
        )
    return (min_id, min_dist)
