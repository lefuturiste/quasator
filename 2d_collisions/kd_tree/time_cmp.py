# sandbox to test for range search

from re import I
from textwrap import indent
import time
import json
import timeit

from cv2 import compare
from ..utils import distance
from .range_search import range_search
from .construct import construct_tree

import random

def naive_range(points, poi, poi_id, radius):
    res = []
    for p in points:
        if p[0] == poi_id: continue
        if distance(p[1:], poi) < radius:
            res.append(p[0])
    return res

def compare_algo(N):
    """
    Goal of the task: find every couple of POI within *warn_radius*
    for N points scattered randomly in a plane
    """
    RADIUS = 0.1
    points = []

    for i in range(N):
        x, y = random.uniform(0, 1), random.uniform(0, 1)
        points.append((i, x, y))

    def naive_implementation():
        incidents = []
        for i in range(N):
            found = naive_range(points, points[i][1:], i, RADIUS)
            for f in found:
                incidents.append((i, f))

    def kd_tree_implementation():
        tree = construct_tree(points)
        incidents = []
        for i in range(N):
            found = range_search(tree, points[i][1:], i, RADIUS)
            for f in found:
                incidents.append((i, f))

    print(f'> With {N} points...')
    print('  naive...')
    nb_of_time = 20
    t1 = timeit.timeit(naive_implementation, number=nb_of_time)
    print('  kd...')
    t2 = timeit.timeit(kd_tree_implementation, number=nb_of_time)
    print('  naive', t1)
    print('  kd_tree', t2)
    print('  diff', abs(t1-t2))

    return {
        'nb_of_points': N,
        'nb_of_runs': nb_of_time,
        'naive': t1,
        'kd_tree': t2
    }

if __name__ == '__main__':
    out = []
    for i in range(0, 4):
        for j in range(1, (4 if i == 3 else 10)):
            n = j*(10**i)
            res = compare_algo(n)
            print(' ', res)
            out.append(res)
    c = json.dumps({
        'created_at': time.time(),
        'data': out
    }, indent=2)
    f = open('./kd_tree_timing_result.json', 'w')
    f.write(c)
    f.close()
 