from .Node import Node

DIM = 2

def construct_tree(points, depth = 0, index = 0, bbox = ([0, 0], [1, 1])):
    """
    Construct the 2d tree
    points has x, y and the id of the point
    """
    if len(points) == 0:
        raise ValueError('Not enough points to work with')
    # get the median
    axis = depth % 2
    points = list(sorted(points, key=lambda p: p[1+axis]))
    med_index = len(points)//2
    id = points[med_index][0]
    med_coord = points[med_index][1:]
    parts = [points[:med_index], points[med_index+1:]]
    children = []
    last = 0
    for i in range(2): # there will be always two cases, two children
        if len(parts[i]) > 0:
            # for the new bbox, only touch the axis along the split
            children.append(construct_tree(
                parts[i],
                depth+1,
                index+1+last,
                (# bbox always  consists of two points
                    [([bbox[0], med_coord][i][j] if j == axis else bbox[0][j]) for j in range(DIM) ],
                    [([med_coord, bbox[1]][i][j] if j == axis else bbox[1][j]) for j in range(DIM) ]
                )
            ))
        last = len(parts[i])
    return Node(
        id,
        index, depth,
        med_coord, axis, bbox, children
    )
