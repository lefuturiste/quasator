from ..utils import distance, project

def range_search_in_child(node, poi, radius: float):
    """
    Internal function used by range_search
    """
    res = []
    if distance(node.point, poi) < radius:
        res.append(node.id)
    if len(node.children) >= 1:
        if distance(project(node.children[0].bbox, poi), poi) < radius:
            res.extend(range_search_in_child(node.children[0], poi, radius))
    if len(node.children) >= 2:
        if distance(project(node.children[1].bbox, poi), poi) < radius:
            res.extend(range_search_in_child(node.children[1], poi, radius))
    return res

def range_search(node, poi, poi_id, radius: float):
    """
    get the list of all point inside a circle centered on POI with a specific radius
    POI = point of interest to search
    """
    # STEP 1: go down the tree to find the given point
    # then try to project this point to many bounding boxes
    # print('cmp', node.id, poi_id, node.id == poi_id)
    if node.id == poi_id:
        # we found the point
        res = []
        # it may have children, so we call the search child on these
        if len(node.children) >= 1:
            res.extend(range_search_in_child(
                node.children[0],
                poi, radius
            ))
        if len(node.children) >= 2:
            res.extend(range_search_in_child(
                node.children[1],
                poi, radius
            ))
        return res
    # dead end
    if not node.has_children(): return []

    # search the fck point
    if poi[node.axis] < node.point[node.axis]:
        side = 0
    if poi[node.axis] > node.point[node.axis]:
        side = 1
    
    res = range_search(node.children[side], poi, poi_id, radius)
    # we found the point, res is a list of neighbours
    # the current point is a parent, we check if it's a neighbour
    if distance(node.point, poi) < radius:
        res.append(node.id)
    
    # we extend the search to the other child
    if len(node.children) == 2:
        res.extend(range_search_in_child(
            node.children[0 if side == 1 else 1],
            poi, radius
        ))
    return res
