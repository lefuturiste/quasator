# sandbox to test for range search

from re import I
import time
import timeit

from cv2 import compare
from ..utils import distance
from .range_search import range_search
from .construct import construct_tree

import random

def naive_range(points, poi, poi_id, radius):
    res = []
    for p in points:
        if p[0] == poi_id: continue
        if distance(p[1:], poi) < radius:
            res.append(p[0])
    return res

def test1():
    points = []

    id_c = 0
    for i in range(500):
        x, y = round(random.uniform(0, 1), 3), round(random.uniform(0, 1), 3)
        if len(list(filter(lambda p: p[1] == x or p[2] == y, points))) > 0:
            continue
        points.append((id_c, x, y))
        id_c += 1

    print(points)

    poi_id = random.randint(0, len(points)-1)
    poi = points[poi_id]
    print('POI is', poi)
    
    tree = construct_tree(points)
    print(tree)

    radius = 0.25
    res1 = range_search(tree, points[poi_id][1:], poi_id, radius)

    res2 = naive_range(points, points[poi_id][1:], poi_id, radius)

    print(res1, res2)
    print(set(res1) == set(res2)) # This should be TRUE!

def compare_algo():
    """
    Goal of the task: find every couple of POI within *warn_radius*
    for N points scattered randomly in a plane
    """
    NB_POINTS = 2000
    RADIUS = 0.1
    points = []

    for i in range(NB_POINTS):
        x, y = random.uniform(0, 1), random.uniform(0, 1)
        points.append((i, x, y))

    def naive_implementation():
        incidents = []
        for i in range(NB_POINTS):
            found = naive_range(points, points[i][1:], i, RADIUS)
            for f in found:
                incidents.append((i, f))

    def kd_tree_implementation():
        tree = construct_tree(points)
        incidents = []
        for i in range(NB_POINTS):
            found = range_search(tree, points[i][1:], i, RADIUS)
            for f in found:
                incidents.append((i, f))

    print('naive...')
    nb_of_time = 10
    t1 = timeit.timeit(naive_implementation, number=nb_of_time)
    print('kd...')
    t2 = timeit.timeit(kd_tree_implementation, number=nb_of_time)
    print('naive', t1)
    print('kd_tree', t2)
    print('diff', abs(t1-t2))


if __name__ == '__main__':
    compare_algo()
