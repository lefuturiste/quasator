from typing import NamedTuple
import copy

class Node(NamedTuple):
    # absolute id known before constructing the tree
    id: str
    # bfs index
    index: int
    depth: int
    # list of coordinate
    point: list
    # index of the coordinate to split
    axis: int
    bbox: tuple
    # list of Node
    children: list

    def has_children(self):
        return len(self.children) > 0

    def without_children(self):
        return Node(self.id, self.index, self.depth, self.point, self.axis, self.bbox, [])
