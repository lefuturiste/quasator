# @see https://www.geeksforgeeks.org/timeit-python-examples/

import timeit

def lol():
    s = 0
    for i in range(300):
        s += 3*i
    return s

print(timeit.timeit(lol, number=200))