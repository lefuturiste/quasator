import * as THREE from "https://unpkg.com/three@0.139.0/build/three.module.js";

import { OrbitControls } from "https://unpkg.com/three@0.139.0/examples/jsm/controls/OrbitControls.js";

let earthGroup, camera, scene, renderer;

const degToRad = (deg) => (deg * Math.PI) / 180;

function getAtmosphereMaterial() {
    const vertexShader = [
        "varying vec3	vVertexWorldPosition;",
        "varying vec3	vVertexNormal;",

        "void main(){",
        "	vVertexNormal	= normalize(normalMatrix * normal);",

        "	vVertexWorldPosition	= (modelMatrix * vec4(position, 1.0)).xyz;",

        "	// set gl_Position",
        "	gl_Position	= projectionMatrix * modelViewMatrix * vec4(position, 1.0);",
        "}",
    ].join("\n")
    const fragmentShader = [
        "uniform vec3	glowColor;",
        "uniform float	coeficient;",
        "uniform float	power;",

        "varying vec3	vVertexNormal;",
        "varying vec3	vVertexWorldPosition;",

        "void main(){",
        "	vec3 worldCameraToVertex= vVertexWorldPosition - cameraPosition;",
        "	vec3 viewCameraToVertex	= (viewMatrix * vec4(worldCameraToVertex, 0.0)).xyz;",
        "	viewCameraToVertex	= normalize(viewCameraToVertex);",
        "	float intensity		= pow(coeficient + dot(vVertexNormal, viewCameraToVertex), power);",
        "	gl_FragColor		= vec4(glowColor, intensity);",
        "}",
    ].join("\n")

    return new THREE.ShaderMaterial({
        uniforms: {
            coeficient: {
                type: "f",
                value: 1.0,
            },
            power: {
                type: "f",
                value: 2,
            },
            glowColor: {
                type: "c",
                value: new THREE.Color("pink"),
            },
        },
        vertexShader,
        fragmentShader,
        //blending	: THREE.AdditiveBlending,
        transparent: true,
        depthWrite: false,
    })
}

function init() {
    scene = new THREE.Scene();

    renderer = new THREE.WebGLRenderer({ antialias: true });
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);
    document.body.appendChild(renderer.domElement);

    // camera

    camera = new THREE.PerspectiveCamera(
        40,
        window.innerWidth / window.innerHeight,
        1,
        1000
    );
    camera.position.set(15, 20, 100);
    scene.add(camera);

    // controls

    const controls = new OrbitControls(camera, renderer.domElement);
    controls.minDistance = 20;
    controls.maxDistance = 800;
    controls.maxPolarAngle = 2 * Math.PI;

    // ambient light, to have minimum lighting
    scene.add(new THREE.AmbientLight(0x222222));

    // directional light to simulate the sun
    const directionalLight = new THREE.DirectionalLight(0xffffff);
    directionalLight.position.set(0, 0, 1).normalize();
    scene.add(directionalLight);

    const helper = new THREE.DirectionalLightHelper(directionalLight, 100);
    scene.add(helper);

    scene.add(new THREE.AxesHelper(30));

    // textures
    earthGroup = new THREE.Group();
    earthGroup.name = 'earthGroup'
    scene.add(earthGroup);

    // points
    const earthRadius = 15;

    let textureLoader = new THREE.TextureLoader();
    const quality = 2;
    let sphereGeometry = new THREE.SphereGeometry(
        earthRadius,
        32 * quality,
        16 * quality
    );
    // adapt to a ellipsoid
    const squishFactor = 6356.7523 / 6378.127; // compute to 0.996

    sphereGeometry.applyMatrix4(
        new THREE.Matrix4().makeScale(1.0, squishFactor, 1.0)
    );

    let material = new THREE.MeshPhongMaterial();

    material.map = textureLoader.load("images/earth_base_map.jpg");
    material.bumpMap = textureLoader.load("images/earth_bump_map.jpg");
    material.bumpScale = 0.05;

    material.specularMap = textureLoader.load("images/earth_specular_map.jpg");
    material.specular = new THREE.Color(
        new THREE.Color("rgb(255,255,255)").multiplyScalar(0.1)
    );

    let earthMesh = new THREE.Mesh(sphereGeometry, material);
    earthMesh.name = 'earthMesh'

    earthGroup.add(earthMesh);


    let lowerAtmosphere = getAtmosphereMaterial()
    lowerAtmosphere.uniforms.glowColor.value.set(0x00b3ff)
    lowerAtmosphere.uniforms.coeficient.value = 0.8
    lowerAtmosphere.uniforms.power.value = 2.0
    let lowerAtmosphereMesh = new THREE.Mesh(sphereGeometry, lowerAtmosphere)
    lowerAtmosphereMesh.scale.multiplyScalar(1.01)
    lowerAtmosphereMesh.name = 'lowerAtmosphereMesh'
    earthGroup.add(lowerAtmosphereMesh)

    let upperAtmosphere = getAtmosphereMaterial()
    upperAtmosphere.side = THREE.BackSide
    upperAtmosphere.uniforms.glowColor.value.set(0x00b3ff)
    upperAtmosphere.uniforms.coeficient.value = 0.5
    upperAtmosphere.uniforms.power.value = 8
    let upperAtmosphereMesh = new THREE.Mesh(sphereGeometry, upperAtmosphere)
    upperAtmosphereMesh.scale.multiplyScalar(1.10)
    upperAtmosphereMesh.name = 'upperAtmosphereMesh'
    earthGroup.add(upperAtmosphereMesh)

    // add a line to show the inclination
    let earthAxis = new THREE.Mesh(
        new THREE.CylinderGeometry(0.25, 0.25, 50, 3),
        new THREE.MeshBasicMaterial({ color: 0xffff00 })
    );
    earthAxis.name = 'earthAxis'
    earthGroup.add(earthAxis);

    let earthContainerGroup = new THREE.Group();
    earthContainerGroup.add(earthGroup);
    scene.add(earthContainerGroup);
    // earth inclination
    const inclination = degToRad(23.4369);
    earthContainerGroup.rotateZ(-inclination);

    // add to earth an athmosphere
    // let atmosphereGeometry = new THREE.SphereGeometry(earthRadius+0.2*earthRadius, 32, 16)
    // let atmosphereMaterial = new THREE.MeshPhongMaterial()
    // atmosphereMaterial.map = textureLoader.load('images/earth_clouds_map.png')
    // atmosphereMaterial.transparent = true
    // atmosphereMaterial.opacity = 0.5
    // let atmosphereMesh = new THREE.Mesh(atmosphereGeometry, atmosphereMaterial)
    // group.add(atmosphereMesh)

    window.addEventListener("resize", onWindowResize);

    window.globalScene = scene
}

function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize(window.innerWidth, window.innerHeight);
}

function animate() {
    requestAnimationFrame(animate);

    earthGroup.rotation.y += 0.005;

    render();
}   

function render() {
    renderer.render(scene, camera);
}

init()
animate()