let gl = null;
let glCanvas = null;

let aVertexPosition;

let uDiagPoints;
let uDiagPointsColors;

let vertexCount;
const vertexNumComponents = 2;
let numberOfPoints = 4;

let vertexBuffers = [];

let params = []
let frames = []
let currentFrame = 0

let fps = 20

let isPlaying = false

const playPauseBtn = document.getElementById('play-pause-btn')
const previousBtn = document.getElementById('previous-frame-btn')
const nextBtn = document.getElementById('next-frame-btn')
const debugInfo = document.getElementById('debug-info')
const resetBtn = document.getElementById('reset-frame-btn')

playPauseBtn.onclick = () => {
    let oldValue = isPlaying
    isPlaying = !isPlaying
    if (!oldValue && isPlaying) {
        // restart the animation
        startAnimation()
    } else {
        stopAnimation()
    }
}

resetBtn.onclick = () => {
    currentFrame = 0
    drawFrame()
}

previousBtn.onclick = () => {
    if (currentFrame == 0) return
    currentFrame--
    drawFrame()
}
nextBtn.onclick = () => {
    if (currentFrame == params.frames-1) return
    currentFrame++
    drawFrame()
}

let intervalLock = 0

const startAnimation = () => {

    intervalLock = setInterval(() => {
        drawFrame()
        if (currentFrame < params.frames-1) {
            currentFrame++
        }
    }, (1/(fps))*1000)

}

const stopAnimation = () => {
    clearTimeout(intervalLock)
}

const constructCircle = (center, radius, precision = Math.PI/4) => {
    let circleData = []
    let [cx, cy] = center
    let r = radius
    for (let a = 0; a<2*Math.PI; a += precision) {
        circleData.push(cx + r*Math.cos(a))
        circleData.push(cy + r*Math.sin(a))
    }
    return circleData
}


// now convert the raw coordinate to a normalized webgl coordinate between -1 and 1
const convertCoordinate = (c) => {
    let max = (2**(8*params.coordinate_size))/2
    if (c < max) {
        return -(1-(c)/max)
    }
    return (c - max)/max
}


const getParameters = async () => {
    let res = await fetch('/parameters')
    let paramsRaw = await res.text()
    let entries = paramsRaw.split("\n").map(p => {
        let c = p.split('=')
        return [c[0], parseInt(c[1])]
    })
    let params = Object.fromEntries(entries.filter(e => e[0] && e[1]))
    return {
        ...params,
        frames: params.fps*params.duration
    }
}

const getFrames = async () => {

    res = await fetch('/scenario_1')

    const reader = res.body.getReader()

    let currentChunk = null;
    let cellPointer = null;
    let loadChunk = async () => {
        res = await reader.read()
        currentChunk = res.value
    }
    const frameCount = params.frames
    let positionHistory = []
    for (let f = 0; f < frameCount; f++) {
        let particulesPositions = new Array(params.particules)
        for (let p = 0; p < params.particules; p++) {
            let coordinates = [0, 0]
            for (let c = 0; c < 2; c++) {
                // unpack coordinate
                let coordinate = 0;
                for (let b = 0; b < params.coordinate_size; b++) {
                    if (cellPointer === null || cellPointer == currentChunk.length) {
                        await loadChunk()
                        cellPointer = 0
                    }
                    coordinate = coordinate | currentChunk[cellPointer] << b*8
                    cellPointer++
                }
                let rawCoordinate = coordinate>>>0
                coordinates[c] = convertCoordinate(rawCoordinate)
            }
            particulesPositions[p] = coordinates
        }
        positionHistory.push(particulesPositions)
    }

    console.log('loaded frames')
    return positionHistory
}

const startup = async (nb = null) => {
    if (nb !== null) {
        numberOfPoints = nb
    }
    glCanvas = document.getElementById("glcanvas");
    gl = glCanvas.getContext("webgl");

    const shaderSet = [
        { type: gl.VERTEX_SHADER, id: "vertex-shader" },
        { type: gl.FRAGMENT_SHADER, id: "fragment-shader" }
    ];

    shaderProgram = buildShaderProgram(shaderSet);
    if (shaderProgram == -1) {
        return
    }

    params = await getParameters()
    console.log(params)
    frames = await getFrames()
    drawFrame()
}

const buildShaderProgram = (shaderInfo) => {
    let program = gl.createProgram();

    shaderInfo.forEach((desc) => {
        let shader = compileShader(desc.id, desc.type);
        if (shader == -1) {
            return -1;
        }

        if (shader) {
            gl.attachShader(program, shader);
        }
    });

    gl.linkProgram(program)

    if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
        console.error("Error linking shader program:", gl.getProgramInfoLog(program));
        return -1;
    }

    return program;
}
  
const compileShader = (id, type) => {
    let code = document.getElementById(id).firstChild.nodeValue;
    let shader = gl.createShader(type);

    gl.shaderSource(shader, code);
    gl.compileShader(shader);

    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        console.error(
            `Error compiling ${type === gl.VERTEX_SHADER ? "vertex" : "fragment"} shader:`,
            gl.getShaderInfoLog(shader)
        );
        return -1;
    }
    return shader;
}

const drawFrame = () => {
    debugInfo.textContent = `${currentFrame}/${frames.length} frame`

    // draw the current frame frame
    gl.viewport(0, 0, glCanvas.width, glCanvas.height);
    gl.clearColor(0.8, 0.9, 1.0, 1.0);
    gl.clear(gl.COLOR_BUFFER_BIT);

    gl.useProgram(shaderProgram);
    
    for (let p = 0; p < params.particules; p++) {
        vertexBuffers[p] = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffers[p]);
        let vertexArray = new Float32Array(
            constructCircle([
                frames[currentFrame][p][0],
                frames[currentFrame][p][1],
            ], 0.01)
        );
        
        gl.bufferData(gl.ARRAY_BUFFER, vertexArray, gl.STATIC_DRAW);
        vertexCount = vertexArray.length / vertexNumComponents;

        gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffers[p]);
        aVertexPosition = gl.getAttribLocation(shaderProgram, "aVertexPosition");
        gl.vertexAttribPointer(aVertexPosition, vertexNumComponents, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(aVertexPosition);
        gl.drawArrays(gl.TRIANGLE_FAN, 0, vertexCount);
    }
}

window.addEventListener("load", () => {
    startup()
}, false);

